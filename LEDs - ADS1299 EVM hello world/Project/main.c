
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stm32f4xx.h>
#include <stm32f4xx_conf.h>
#include <stm32f4xx.h>
#include <misc.h>			 // I recommend you have a look at these in the ST firmware folder
#include <stm32f4xx_usart.h> // under Libraries/STM32F4xx_StdPeriph_Driver/inc and src

uint16_t getdata;  //global variable to pass SPI data
uint16_t getIDval; //global variable to store ADS1299 ID val.
int gMaxChan = 0;


int main(void) {
///////// Initial Portion /////////////////////////
		init_USART1(9600); // initialize USART1 @ 9600 baud
		SystemCoreClockUpdate();                      /* Get Core Clock Frequency   */
		if (SysTick_Config(SystemCoreClock / 1000000)) { /* SysTick 1 micro sec interrupts  */
			while (1);                                  /* Capture error              */
		}
		USART_puts(USART1, "START !"); // just send a message to indicate that it works

		//Init GPIO for Leds on board and button input
		init_GPIO();
	
		//Init SPI1
		mySPI_Init();
	
		//Delay for all periferals to be ready
		Delay(50000);							//Delay 500ms for cap to be charged
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
		GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
			Delay(1000000L );
			
		
		//Send stop command for ADS1299
		/*Put CS line down
		send out command (stop command) SDATAC = 0x11,
		delay 1 micro seccond;
		put CS line up again*/
		mySPI_SendByte(0x11);  //SDATAC
		Delay(10000);
		getIDval = mySPI_GetData(0x20); // Read register at address 0x00 (ID)		
		switch (getIDval & 0x1F ) { //least significant bits reports channels
          case  0x10: //16
            gMaxChan = 4; //ads1294
            break;
          case 0x11: //17
            gMaxChan = 6; //ads1296
            break; 
          case 0x12: //18
            gMaxChan = 8; //ads1298
            break;
          case 0x1E: //30
            gMaxChan = 8; //ads1299
						USART_puts(USART1, "ADS1299 Hooray!");
						while(1)
						{
							GPIO_ToggleBits(GPIOC, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
							Delay(100000L );
						}
							
					break;
          default: 
            gMaxChan = 0;
		}
	/////////////////- Loop - ////////////////////////////////////
 while (1){
    
     // You can do whatever you want in here
		/*
		USART_puts(USART1, "Init 2 complete! Hello World! \r \n"); // just send a message to indicate that it works
				
		//GPIO_ToggleBits(GPIOD, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
		GPIO_ToggleBits(GPIOD, GPIO_Pin_12 ); //Test Leds on board
		Delay(200);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_13 ); //Test Leds on board
		Delay(200);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_14 ); //Test Leds on board
		Delay(1000);
     */
		GPIO_ToggleBits(GPIOD, GPIO_Pin_12 ); //Test Leds on board
		Delay(100000);
		getdata = mySPI_GetData(0x00);
  }
//////////////////////////////////////////////////////////	
}




#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

function varargout = SPCRS232(varargin)
% SPCRS232 M-file for SPCRS232.fig
%      SPCRS232, by itself, creates a new SPCRS232 or raises the existing
%      singleton*.
%
%      H = SPCRS232 returns the handle to a new SPCRS232 or the handle to
%      the existing singleton*.
%
%      SPCRS232('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPCRS232.M with the given input arguments.
%
%      SPCRS232('Property','Value',...) creates a new SPCRS232 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SPCRS232_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SPCRS232_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SPCRS232

% Last Modified by GUIDE v2.5 02-Jan-2014 20:15:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SPCRS232_OpeningFcn, ...
                   'gui_OutputFcn',  @SPCRS232_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SPCRS232 is made visible.
function SPCRS232_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SPCRS232 (see VARARGIN)

% Choose default command line output for SPCRS232
handles.output = hObject;
delete(instrfindall);
% Update handles structure
guidata(hObject, handles);
save('handles.mat', 'handles');

% UIWAIT makes SPCRS232 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SPCRS232_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in ListPortname.
function ListPortname_Callback(hObject, eventdata, handles)
% hObject    handle to ListPortname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListPortname contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListPortname
% PortNumber=get(handles.ListPortname,'Value');
% Portname=['COM',num2str(PortNumber)];
%set(handles.TextNumber,'String',str);

% --- Executes during object creation, after setting all properties.
function ListPortname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListPortname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListBaudrate.
function ListBaudrate_Callback(hObject, eventdata, handles)
% hObject    handle to ListBaudrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListBaudrate contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListBaudrate


% --- Executes during object creation, after setting all properties.
function ListBaudrate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListBaudrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListDatabits.
function ListDatabits_Callback(hObject, eventdata, handles)
% hObject    handle to ListDatabits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListDatabits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListDatabits

% --- Executes during object creation, after setting all properties.
function ListDatabits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListDatabits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListParity.
function ListParity_Callback(hObject, eventdata, handles)
% hObject    handle to ListParity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListParity contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListParity

% --- Executes during object creation, after setting all properties.
function ListParity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListParity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListStopbits.
function ListStopbits_Callback(hObject, eventdata, handles)
% hObject    handle to ListStopbits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListStopbits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListStopbits


% --- Executes during object creation, after setting all properties.
function ListStopbits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListStopbits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);

% --- Executes on button press in ButtonConnect.
function ButtonConnect_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)Portname =
% getCurrentPopupSjhtring(handles.ListPortname);
% SerialPort=serial(getCurrentPopupString(ListPortname));
global dataall
dataall=[];
global data1
data1=[];
global data2
data2=[];
% set(handles.TextPortdata,'String',getCurrentPopupString(ListPortname));
a=get(handles.ButtonConnect,'String');
set(handles.TextPortdata,'String',a);
if strcmp(a,'Connect')
    assignin('base', 't0', 0); % tao trong workspace bien t0=0 de chuan bi lam 1 data moi
    Portname=getCurrentPopupString(handles.ListPortname);
    SerialPort=serial(Portname);
    SerialPort.Baudrate=str2num(getCurrentPopupString(handles.ListBaudrate));
    SerialPort.Databits=str2num(getCurrentPopupString(handles.ListDatabits));
    SerialPort.Parity=getCurrentPopupString(handles.ListParity);
    SerialPort.Stopbits=get(handles.ListStopbits,'Value');
    SerialPort.Baudrate=str2num(getCurrentPopupString(handles.ListBaudrate));
    SerialPort.InputBufferSize=5000000;
    %Nget=1;
    SerialPort.BytesAvailableFcnCount = 1;
%   SerialPort.BytesAvailableFcnMode = 'terminator';
    SerialPort.BytesAvailableFcnMode = 'byte';
%   SerialPort.BytesAvailableFcn = @BytesAvailable_Callback;
    %axis(handles.axes1,[xmax-4800 xmax -10 10]);
    
    hold on;
    plotHandle=plot(handles.axes2,0,'-b','LineWidth',2);
    SerialPort.BytesAvailableFcn = {@localReadAndPlot,plotHandle,1};
    
    try
       handles.SerialPort = SerialPort; % s chinh la handles.s 
       fopen(handles.SerialPort);
       % hien thi Disconnect
       set(handles.ButtonConnect, 'String','Disconnect')
       drawnow;
    catch e
       if(strcmp(handles.SerialPort.status,'open')==1)
           fclose(handles.SerialPort);
       end
       errordlg(e.message); % xu ly loi ngoai le, neu khong co ngoai le xay ra thi se thuc hien catch
    end
        
else
    set(handles.ButtonConnect, 'String','Connect')
    fclose(handles.SerialPort);
end
guidata(hObject, handles); % hObject la cai hien tai

function localReadAndPlot(interfaceObject,~,figureHandle,bytesToRead)
% 
global dataall
global data1
global data2
% Read the desired number of data bytes
data = fread(interfaceObject,bytesToRead); 
dataall=[dataall;data];

data1=[data1;dataall(2*(length(data1)+1):2:length(dataall))];
data2=[data2;dataall(2*length(data2)+1:2:length(dataall))];

if (length(data1)>2000)&&(length(data2)>2000)
    y=data1(end-2000:end);
    z=data2(end-2000:end);
else
    y=data1;
    z=data2;
end
%save('mydata',data(x));
% Update the plot
set(figureHandle,'Ydata',y,'Zdata',z);

% --- Executes during object creation, after setting all properties.
function ButtonConnect_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ButtonConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in ButtonRead.
function ButtonRead_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonRead (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% global SerialPort
% global data
% global stopplot
% t=0;
% while stopplot==0
% data=[data;fread(SerialPort,SerialPort.InputBufferSize)];
% t=t+SerialPort.InputBufferSize;
% set(handles.TextPortdata,'String',num2str(data'));
% set(handles.axes2,'NextPlot','add'); % lenh hold on trong GUI
% plot(handles.axes2,data(t:end));
% drawnow


% --- Executes on button press in ButttonExit.
function ButttonExit_Callback(hObject, eventdata, handles)
% hObject    handle to ButttonExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load handles
fclose(obj);
set(handles.TextPortstatus,'String',obj.Status);

%%%%%%%%%%%%%%%% SUPPORT FUNCTION [1]
function str = getCurrentPopupString(hh)
%# getCurrentPopupString returns the currently selected string in the popupmenu with handle hh

%# could test input here
if ~ishandle(hh) || strcmp(get(hh,'Type'),'popupmenu')
error('getCurrentPopupString needs a handle to a popupmenu as input')
end

%# get the string - do it the readable way
list = get(hh,'String');
val = get(hh,'Value');
if iscell(list)
   str = list{val};
else
   str = list(val,:);
end

%%%%%%%%%%%%%%%%%SUPPORT FUNCTION [2]
function BytesAvailable_Callback(obj,event) 
load handles
persistent  t0;
persistent  nPts;
persistent  xTime;
persistent  yDataCH1;
 
persistent  nDataCH1Save;
persistent  nDataCH2Save;
 
data = fread(obj,obj.BytesAvailable);
t0 = evalin('base', 't0');
if t0==0
    nPts = 38000;	% number of points to display on stripchart
    xTime = ones(1,nPts)*NaN;
    yDataCH1 = ones(1,nPts)*NaN;
end
lenFrameData=length(data);
% khoi tao data
nIRData = [];
nRData = [];
 % tach data thanh cac luong du lieu
synIndex = 1; % contro data
while (synIndex < (lenFrameData - 12))
     if ((data(synIndex)==255)&&(data(synIndex+1)==255)&&(data(synIndex+2)==0)&&(data(synIndex+3)==0)&&(data(synIndex+4)==255))
         nIRData = [nIRData (data(synIndex+8)*256+data(synIndex+7))]; % IR truoc
         nRData = [nRData  (data(synIndex+10)*256+data(synIndex+9))];% R  sau
         synIndex = synIndex + 11; % bo qua khung vua roi
     else
         synIndex = synIndex + 1;  % do dong bo
     end
     
end
%--------------save Data--------------------------------------------
nDataCH1Save=[nDataCH1Save nIRData];
nDataCH2Save=[nDataCH2Save nRData];
  
assignin('base', 'nDataCH1Save', nDataCH1Save); % tao trong workspace
assignin('base', 'nDataCH2Save', nDataCH2Save); % tao trong workspace
lenData = length(nRData); % du lieu that data

% Update the plot, initial t0=0 in workspace
% t1=length(TimeSecond);
time=t0:1:t0+lenData-1;
t0=t0+lenData-1; % update thoi diem luc sau
assignin('base', 't0', t0); % tao trong workspace

xTime(1:end-lenData) = xTime(lenData+1:end);  % shift old data left
xTime(end-lenData+1:end) = time;        % new data goes on right

% yPos1 = get(handles.yPos1,'value');
% yDataCH1(1:end-lenData) = yDataCH1(lenData+1:end);  % shift old data left
% yDataCH1(end-lenData+1:end) = (yPos1 + 2.5) + 120*nDataCH1f0DC*2.5/32767;%(nDataf0DC*2.5/32768)*2;        % new data goes on right 75
yDataCH1(1:end-lenData) = yDataCH1(lenData+1:end);
assignin('base', 'yDataCH1', yDataCH1); % tao trong workspace
assignin('base', 'yDataCH2', yDataCH2); % tao trong workspace
assignin('base', 'xTime', xTime); % tao trong workspace
 
xmin=min(xTime);
xmax=max(xTime);
 
if (xmin==NaN)
    xmax=1;
    xmin=0;
end
set(handles.axes1,'NextPlot','add'); % lenh hold on trong GUI
     %set(handles.axes1,'NextPlot','new'); % lenh new
    
     plot(handles.axes1,xTime,yDataCH1,'b-',xTime,yDataCH2,'r-','LineWidth',2) ;
     
     %plot(handles.axes1,xTime,(yDataCH1+yDataCH2),'r-','LineWidth',2) ;
     %plot(handles.axes1,xTime,yDataCH2,'r-') ;

     axis(handles.axes1,[xmax-4800 xmax -10 10]);
     %axis(handles.axes1,[xmin xmax -1 1]);

     drawnow                                 % refresh display
     % refreshdata(handles.axes1);
     save('handles.mat', 'handles');  


% --- Executes on key press with focus on ButtonConnect and none of its controls.
function ButtonConnect_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to ButtonConnect (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

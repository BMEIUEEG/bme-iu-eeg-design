#include <stm32f4xx.h>
#include <misc.h>
#include "Usart.h"
#include "SysTick.h"
#include "Leds.h"
#include "SPI.h"
#include "ADS_Command.h"
#include <string.h>
#include "stdio.h"
	uint8_t countbytes, countbits,data[10][10],i;
	uint32_t dataChn[10];
	
int main(void) 
{
  Init_USART(192000); // initialize USART1 @ 9600 baud
	Led_Init();
	InitialiseSysTick();
	mySPI_Init();
	
	PowerUp_ADS(); 						// Power up ADS
	SendADSCommand(0x11,2);		// Send Command SDATA: STOP RDATAC mode
	Recognize_ADS();
	
	WREG_ADS(0x01,0xD6);			//Config1: Daisy Chain Mode, Oscillator output disable, DT 250SPS
	WREG_ADS(0x02,0xC0);			//Config2: Test Signals are driven Externally, Test Signal and Amplitude are Default
	WREG_ADS(0x03,0xE1);			//Config3: Unsing Internal Reference
	WREG_ADS(0x05,0x00);			//Channel 1-8: Normal Operation;PGA=1;SRB2 off;Normal electrode input
	WREG_ADS(0x06,0x00);			//-
	WREG_ADS(0x07,0x00);			//-
	WREG_ADS(0x08,0x00);			//-3
	WREG_ADS(0x09,0x00);			//-
	WREG_ADS(0x0A,0x00);			//-
	WREG_ADS(0x0B,0x00);			//-
	WREG_ADS(0x0C,0x00);			//-8
	//* Square wave test signal 
/*
	WREG_ADS(0x02,0xD0);			//
	WREG_ADS(0x05,0x05);
	WREG_ADS(0x06,0x05);
	WREG_ADS(0x07,0x05);
	WREG_ADS(0x08,0x05);
	WREG_ADS(0x09,0x05);
	WREG_ADS(0x0A,0x05);
	WREG_ADS(0x0B,0x05);
	WREG_ADS(0x0C,0x05);
*/
	GPIO_SetBits(GPIOB, GPIO_Pin_1); //Enable START, then /DRDY toggle at f_CLK/8192 =>t=4ms
	Delay(10);	
	GetData_RDATAC();


/*
	while(1)
	{	
		//Send_Header(0xfe,0x01);
		GetData_RDATAC();
		//GetData_RDATA();							//Retrive Data from RDATA mode
		
	}
*/
}
 

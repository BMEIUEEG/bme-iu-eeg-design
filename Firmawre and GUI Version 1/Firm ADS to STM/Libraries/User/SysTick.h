#ifndef __SysTick_H
#define __SysTick_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include <stm32f4xx.h>
#include <misc.h>			 
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void InitialiseSysTick(void);
void Delay(__IO uint32_t nTime);
void SysTick_Handler(void);

#endif 


#include "Leds.h"

void Led_Init(void)
{
	//Initialize 4LED on STM32F4 for checking
	GPIO_InitTypeDef Led_Init;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	Led_Init.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15; // we want to configure all LED GPIO pins
	Led_Init.GPIO_Mode = GPIO_Mode_OUT; 		// we want the pins to be an output
	Led_Init.GPIO_Speed = GPIO_Speed_50MHz; 	// this sets the GPIO modules clock speed
	Led_Init.GPIO_OType = GPIO_OType_PP; 	// this sets the pin type to push / pull (as opposed to open drain)
	Led_Init.GPIO_PuPd = GPIO_PuPd_NOPULL; 	// this sets the pullup / pulldown resistors to be inactive
	GPIO_Init(GPIOD, &Led_Init); 			// this finally passes all the values to the GPIO_Init function which takes care of setting the corresponding bits.
	//GPIO_SetBits(GPIOD, GPIO_Pin_15);
}


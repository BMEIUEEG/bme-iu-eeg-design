#ifndef __SPI_H
#define __SPI_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include <stm32f4xx.h>
#include <misc.h>			 
#include <stm32f4xx_spi.h>
#include "SysTick.h"
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void mySPI_SendCommand(uint8_t data, uint8_t delaytime);
void mySPI_SendData(uint8_t address, uint8_t data); 
uint16_t mySPI_GetData(uint8_t address);
void mySPI_Init(void);
#endif /* __SPI_H */

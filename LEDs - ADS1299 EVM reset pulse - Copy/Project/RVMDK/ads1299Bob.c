#include "stm32f4xx.h"
#include <stm32f4xx_usart.h> // under Libraries/STM32F4xx_StdPeriph_Driver/inc and src
#include <stdio.h>
#include <main.h>


int i =0; //counter
int regAddress =0;
char str[5];
uint8_t getDataSPI;
	
	
	void readRegister(uint8_t regCount, uint8_t startAddress)
	{
	//Read all register data
		 for(i = 0; i < regCount; i++)
       {
				 regAddress = (0x20 | i);
				 getDataSPI = mySPI_Send3Byte((0x20 | i),0x00,0x00);								//Increase to required address from the base address;
																																				//Read 1 register at a time
				 snprintf(str, 5, "%X", i);		//convert int to string
				 
				 if (i ==0){
					 USART_puts(USART1, "Register Map and Value \r \n");					//Header of register map
				 }
				 //Address part
				 USART_puts(USART1, "Address: ");
				 USART_puts(USART1, str);					//pirnt the address in string format
				 //Value part
				 USART_puts(USART1, "			Value : ");
				 snprintf(str, 5, "%X", getDataSPI);		//convert int to string
				 USART_puts(USART1, str);				//Print register value in string
				 USART_puts(USART1, " \r \n");			// new line
			 }
	}
	
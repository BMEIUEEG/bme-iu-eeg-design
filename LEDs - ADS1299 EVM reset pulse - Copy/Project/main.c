
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stm32f4xx.h>
#include <stm32f4xx_conf.h>
#include <stm32f4xx.h>
#include <misc.h>			 // I recommend you have a look at these in the ST firmware folder
#include <stm32f4xx_usart.h> // under Libraries/STM32F4xx_StdPeriph_Driver/inc and src

uint32_t getdata;  //global variable to pass SPI data
uint16_t getIDval; //global variable to store ADS1299 ID val.
int gMaxChan = 0;


int main(void) {
///////// Initial Portion /////////////////////////
		init_USART1(460800); // initialize USART1 @ 9600 baud
		SystemCoreClockUpdate();                      /* Get Core Clock Frequency   */
		if (SysTick_Config(SystemCoreClock / 1000000)) { /* SysTick 1 micro sec interrupts  */
			while (1);                                  /* Capture error              */
		}
		USART_puts(USART1, "START !"); // just send a message to indicate that it works
		USART_puts(USART1, " \r \n");			// new line
		//Init GPIO for Leds on board and button input
		init_GPIO();
	
		//Init SPI1
		mySPI_Init();
		
		//Delay for all periferals to be ready
		Delay(1000000);							//Delay 1s for cap to be charged
	//	mySPI_SendByte(0x11);  //SDATAC
		//mySPI_Send3Byte(0x20,0x00,0x00);
		
		//Issue Reset Pulse for ADS1299
		GPIO_ResetBits(GPIOD, GPIO_Pin_2);
		Delay(5); 																			//Delay couples of clock cycles for ADS to read the signal
		GPIO_SetBits(GPIOD, GPIO_Pin_2);								// Complete reset Pulse
		Delay(18);																			//Delay at least 18 clock cycles
		
		//Send stop command for ADS1299
		/*Put CS line down
		send out command (stop command) SDATAC = 0x11,
		delay 1 micro seccond;
		put CS line up again*/
		mySPI_SendByte(0x11);  //SDATAC
		Delay(10000);
		//�WREG CONFIG3 E0h
		// mySPI_SendData(0x43); //Configure register
		mySPI_Send3Byte(0x43,0x00,0xE1);								//config3 ENABLE internal reference
		mySPI_Send3Byte(0x41,0x00,0xD6);								// Config1 noi daisy chain; output data rate 250sps(F/4096)			
		mySPI_Send3Byte(0x42,0x00,0xC0);								//Config 2: test source externa, keep all as default
		
		//Write to 8 channel open the shorted
		mySPI_Send3Byte(0x45,0x00,0x00);								//collecting data for channel 1-4
		mySPI_Send3Byte(0x46,0x00,0x00);	
		mySPI_Send3Byte(0x47,0x00,0x00);	
		mySPI_Send3Byte(0x48,0x00,0x00);	
				

	
	
	mySPI_Send3Byte(0x20,0x00,0x00);
		
		switch (getIDval & 0x1F ) { //least significant bits reports channels
          case  0x10: //16
            gMaxChan = 4; //ads1294
            break;
          case 0x11: //17
            gMaxChan = 6; //ads1296
            break; 
          case 0x12: //18
            gMaxChan = 8; //ads1298
            break;
          case 0x1E: //30
            gMaxChan = 8; //ads1299
						USART_puts(USART1, "ADS1299 Hooray!");	
					break;
          default: 
            gMaxChan = 0;
		}
		
		
		readRegister(18,0);												// Read all register from address 0
	
	
		//Start conversation. Set start pin to 1 
		GPIO_SetBits(GPIOD, GPIO_Pin_0);
		
			// Asking for data to send continueously 
		mySPI_SendByte(0x10);  //SDATAC begin to read data
		
	/////////////////- Loop - ////////////////////////////////////
 while (1){
	
	
}// while (1)
	 
//////////////////////////////////////////////////////////	
} //main






/////////Interrupt handler
void EXTI1_IRQHandler(void)
{
  uint8_t i =0;
	if(EXTI_GetITStatus(EXTI_Line1) != RESET)
  {
    /* Toggle LED1 */
    
		GPIO_ToggleBits(GPIOD, GPIO_Pin_12); //Check if interrupt is working
    
		
    if(!(GPIO_ReadInputData(GPIOD) &  GPIO_Pin_1)){            //Read state of D1 (DRDY signal). Start new process if it is 0
		for( i=0; i<9; i++){
		char strADS[50];	
		 getdata = mySPI_GetData(0x00);
		 		// regAddress = (0x20 | i);
				// getDataSPI = mySPI_Send3Byte((0x20 | i),0x00,0x00);								//Increase to required address from the base address;
			if( i==2){																																	//Read 1 register at a time
		 if (getdata > 0x007FFFFF) //Convert minus data
		 {
			getdata = 0xFFFFFF - getdata;
			getdata = getdata/19373;
		//snprintf(strADS, 50, "Channel %d		Val: %X \r \n", i, getdata);		//convert int to string
		 snprintf(strADS, 50, "E0,-%d\n",getdata);		//convert int to string
		 USART_puts(USART1, strADS);					//pirnt the address in string format
		 }
			else{ 
		getdata = getdata/19373;
		//snprintf(strADS, 50, "Channel %d		Val: %X \r \n", i, getdata);		//convert int to string
		 snprintf(strADS, 50, "E0,%d\n",getdata);		//convert int to string
		USART_puts(USART1, strADS);					//pirnt the address in string format
			}
			}
			
		}
		//Delay(50000);
  } //if
		
		
		
		
		
		
		
    /* Clear the EXTI line 1 pending bit */
    EXTI_ClearITPendingBit(EXTI_Line1);
  }
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

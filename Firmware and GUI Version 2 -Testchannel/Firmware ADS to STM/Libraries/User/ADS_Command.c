#include "ADS_Command.h"

//****** Power Up ADS  ******
void PowerUp_ADS(void)
{
	//** Power Up and Digital Power Up **
	Delay(1000000);					//Delay 1s for Power Up
	//GPIO_ResetBits(GPIOE, GPIO_Pin_5);  // Enable /CS to start supplying Master Clock Signal
	Delay(400000);					// Delay t_por = 2^16 x t_CLK of ADS (1/2.048MHz)=0.032s => Delay 40ms
	GPIO_ResetBits(GPIOD, GPIO_Pin_2); //Transmit a RESET Pulse
	Delay(1);								// Delay 2 x t_CLK of ADS = 2x(1/2.048MHz)=0.977us
	GPIO_SetBits(GPIOD, GPIO_Pin_2);   //Stop RESET pulse	 
	Delay(10);							// Delay 18 x t_CLK of ADS = 18x(1/2.048MHz)=8.789us to start using the Device
	Delay(1000000);  				//Delay 1s for Power-On Reset 
	//GPIO_SetBits(GPIOE, GPIO_Pin_5); //Disable /CS: SPI1;
	//** End of Power Up and Digital Power Up **	
}

//****** Regcognize ADS1299  ******
uint16_t ADS_ID, reg_count;
void Recognize_ADS(void)
{
	ADS_ID=RREG_ADS(0x00);
	if ((ADS_ID & 0x0f)==0x0e)
	{
		for (reg_count=0;reg_count<6;reg_count++)
		{
			GPIO_ToggleBits(GPIOD,GPIO_Pin_12);   		//Display LED to indicate that connection is OK!
			Delay(500000);
		}
	}
}

//****** GetData in RDATA mode  ******
void GetData_RDATA(void)
{
	uint8_t countbytes,countbits;
	uint32_t data_raw[10][10];
	uint32_t dataChn[10];
	
	while (GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_1)!=0);		//Catch /DRDY
	GPIO_ResetBits(GPIOD, GPIO_Pin_10);      								//Enable /CS
	
	// Send Command RDATA: send RDATA opcode
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, 0x12);														//Send Opcode RDATA
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);
	
	// Collect 9*8*3 = 216 bits
	for (countbytes=1;countbytes<10;countbytes++)
	{
		for (countbits=3;countbits>0;countbits--)
		{
			while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
			SPI_I2S_SendData(SPI1, 0x00);														//send Dummy byte	
			while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
			data_raw[countbytes][countbits]=SPI_I2S_ReceiveData(SPI1);												//Recieve data from SPI1
			//while( !(USART1->SR & 0x00000040));
			//USART_SendData(USART1, data_raw[countbits]);
			dataChn[countbytes]=dataChn[countbytes]|(data_raw[countbytes][countbits]<<((countbits-1)*8));
		}
		if (countbytes!=1)
		{
			if (dataChn[countbytes]>0x007fffff)
			{
				dataChn[countbytes]=0x00ffffff-dataChn[countbytes];
			}
			else 
			{
				dataChn[countbytes]=dataChn[countbytes]+0x00800000;	
			}
		}
	}
	for (countbytes=1;countbytes<10;countbytes++)
	{
		Send_Data_uint24(dataChn[countbytes]);
	}
}

//****** GetData in RDATAC mode  ******
void GetData_RDATAC(void)
{
	uint32_t countall=0;
	SendADSCommand(0x10,2);
	while(1)
	{
	uint8_t countbytes,countbits;
	uint32_t data[10][10],dataChn[10];
		
	while(GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_1)!=0);
	GPIO_ResetBits(GPIOD, GPIO_Pin_10); 		
	for (countbytes=1;countbytes<10;countbytes++)
	{
		for (countbits=1;countbits<4;countbits++)
		{
			while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
			SPI_I2S_SendData(SPI1, 0x00);												//send Opcode1
			while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
			data[countbytes][countbits]=SPI_I2S_ReceiveData(SPI1);
		}
	}
	GPIO_SetBits(GPIOD, GPIO_Pin_10);
	Send_Header(0xfe,0x01);
 		
	for (countbytes=1;countbytes<10;countbytes++)
	{			
		dataChn[countbytes]=0;
		for (countbits=1;countbits<4;countbits++)
		{
			dataChn[countbytes]=dataChn[countbytes]|(data[countbytes][countbits]<<((3-countbits)*8));
		}
		if(countbytes!=1)
		{
		if (dataChn[countbytes]<0x00800000)
			{
				dataChn[countbytes]=dataChn[countbytes]+0x00800000;
			}
			else
			{
				dataChn[countbytes]=dataChn[countbytes]-0x00800000;
			}
		}
		Send_Data_uint24(dataChn[countbytes]);
	}
	countall++; //count number of data send
	Send_Data_uint32(countall);
	
	}
}

//****** RREG Command ADS  ******
uint16_t RREG_ADS(uint8_t address)
{
		uint8_t opcode1, opcode2;
	  opcode1 = address | 0x20; 															//Opcode 010r rrrr
	  opcode2 = 0x00;																					//Opcode 000n nnnn =0 : write n channels
		GPIO_ResetBits(GPIOD, GPIO_Pin_10); 											//CS of SPI1: enable ADS - /CS is connected to PC5
	  
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
		SPI_I2S_SendData(SPI1, opcode1);												//send Opcode1
	
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
		SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1

		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
		SPI_I2S_SendData(SPI1, opcode2);												//Send Opcde2
	
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
		SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1 
		
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
		SPI_I2S_SendData(SPI1, 0x00);														//Send dummny bytes
	
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
		SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1
		
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE)); 
		GPIO_SetBits(GPIOD, GPIO_Pin_10);												//CS of SPI1: turn off SPI1
		
		Delay(15);
		return SPI_I2S_ReceiveData(SPI1); 											//return reveiced data
		
}

//****** WREG Command ADS  ******
void WREG_ADS(uint8_t address,uint8_t data)
{
		uint8_t opcode1, opcode2;
		opcode1 = address | 0x40; 			//Opcode 010r rrrr
	  opcode2 = 0x00;								//Opcode 000n nnnn =0 : write n channels
	  
		GPIO_ResetBits(GPIOD, GPIO_Pin_10); 											//CS of SPI1: enable ADS - /CS is connected to PC5
	  while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
		SPI_I2S_SendData(SPI1, opcode1);												//send Opcode1
	
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
		SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1

		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
		SPI_I2S_SendData(SPI1, opcode2);												//Send Opcde2
	
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
		SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1 
		
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
		SPI_I2S_SendData(SPI1, data);														//Send data
	
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
		SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1 
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));
		
		Delay(15);																							//When WREG CONFIG1 =>auto Reset => need to delay 18*t_CLK=~9us		
		GPIO_SetBits(GPIOD, GPIO_Pin_10); 												//CS line up; Disable slave???
}

//****** Send other Command to ADS with appropriated time  ******
void SendADSCommand(uint8_t data, uint32_t delaytime)
{
		GPIO_ResetBits(GPIOD, GPIO_Pin_10);  // Enable /CS to start supplying Master Clock Signal
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
		SPI_I2S_SendData(SPI1, data);
		while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
		SPI_I2S_ReceiveData(SPI1);
	
		Delay(delaytime);
		GPIO_SetBits(GPIOD, GPIO_Pin_10);												//CS of SPI1: turn off SPI1
}

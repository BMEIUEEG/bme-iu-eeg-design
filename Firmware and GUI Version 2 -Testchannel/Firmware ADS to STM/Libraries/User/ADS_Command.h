#ifndef __ADS_Command_H
#define __ADS_Command_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include <stm32f4xx.h>
#include <misc.h>			 
#include <stm32f4xx_spi.h>
#include "SysTick.h"
#include "Usart.h"
#include "SPI.h"
#include "Leds.h"

/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void GetData_RDATAC(void);
uint16_t RREG_ADS(uint8_t address);
void WREG_ADS(uint8_t address,uint8_t data);
void PowerUp_ADS(void);
void Recognize_ADS(void);
void SendADSCommand(uint8_t data, uint32_t delaytime);
void GetData_RDATA(void);
#endif /* __ADS_Command_H */

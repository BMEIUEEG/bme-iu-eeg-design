function varargout = SPCRS232(varargin)
% SPCRS232 M-file for SPCRS232.fig
%      SPCRS232, by itself, creates a new SPCRS232 or raises the existing
%      singleton*.
%
%      H = SPCRS232 returns the handle to a new SPCRS232 or the handle to
%      the existing singleton*.
%
%      SPCRS232('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPCRS232.M with the given input arguments.
%
%      SPCRS232('Property','Value',...) creates a new SPCRS232 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SPCRS232_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SPCRS232_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SPCRS232

% Last Modified by GUIDE v2.5 05-Jun-2014 16:10:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SPCRS232_OpeningFcn, ...
                   'gui_OutputFcn',  @SPCRS232_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SPCRS232 is made visible.
function SPCRS232_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SPCRS232 (see VARARGIN)

% Choose default command line output for SPCRS232
handles.output = hObject;
delete(instrfindall);
% Update handles structure
guidata(hObject, handles);
save('handles.mat', 'handles');

% UIWAIT makes SPCRS232 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SPCRS232_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in ListPortname.
function ListPortname_Callback(hObject, eventdata, handles)
% hObject    handle to ListPortname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListPortname contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListPortname
% PortNumber=get(handles.ListPortname,'Value');
% Portname=['COM',num2str(PortNumber)];
%set(handles.TextNumber,'String',str);

% --- Executes during object creation, after setting all properties.
function ListPortname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListPortname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListBaudrate.
function ListBaudrate_Callback(hObject, eventdata, handles)
% hObject    handle to ListBaudrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListBaudrate contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListBaudrate


% --- Executes during object creation, after setting all properties.
function ListBaudrate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListBaudrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListDatabits.
function ListDatabits_Callback(hObject, eventdata, handles)
% hObject    handle to ListDatabits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListDatabits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListDatabits

% --- Executes during object creation, after setting all properties.
function ListDatabits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListDatabits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListParity.
function ListParity_Callback(hObject, eventdata, handles)
% hObject    handle to ListParity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListParity contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListParity

% --- Executes during object creation, after setting all properties.
function ListParity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListParity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListStopbits.
function ListStopbits_Callback(hObject, eventdata, handles)
% hObject    handle to ListStopbits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListStopbits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListStopbits


% --- Executes during object creation, after setting all properties.
function ListStopbits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListStopbits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);

% --- Executes on button press in ButtonConnect.
function ButtonConnect_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)Portname =
% getCurrentPopupSjhtring(handles.ListPortname);
% SerialPort=serial(getCurrentPopupString(ListPortname));
global dataall;dataall=[];global datac;datac=[];
global data1;data1=[];global data2;data2=[];
global data3;data3=[];global data4;data4=[];
global data5;data5=[];global data6;data6=[];
global data7;data7=[];global data8;data8=[];

a=get(handles.ButtonConnect,'String');
if strcmp(a,'Connect')
    assignin('base', 't0', 0); % tao trong workspace bien t0=0 de chuan bi lam 1 data moi
    Portname=getCurrentPopupString(handles.ListPortname);
    SerialPort=serial(Portname);
    SerialPort.Baudrate=str2num(getCurrentPopupString(handles.ListBaudrate));
    SerialPort.Databits=str2num(getCurrentPopupString(handles.ListDatabits));
    SerialPort.Parity=getCurrentPopupString(handles.ListParity);
    SerialPort.Stopbits=get(handles.ListStopbits,'Value');
    SerialPort.Baudrate=str2num(getCurrentPopupString(handles.ListBaudrate));
    %SerialPort.InputBufferSize=500000;
    SerialPort.BytesAvailableFcnCount = 150;
%   SerialPort.BytesAvailableFcnMode = 'terminator';
    SerialPort.BytesAvailableFcnMode = 'byte';
        
    Channel=str2num(getCurrentPopupString(handles.ListChan));  %%get Channel to plot
    SerialPort.BytesAvailableFcn = {@localReadAndPlot,Channel,150};
    try
       handles.SerialPort = SerialPort; % s chinh la handles.s 
       fopen(handles.SerialPort);
       % hien thi Disconnect
       set(handles.ButtonConnect, 'String','Disconnect')
       drawnow;
    catch e
       if(strcmp(handles.SerialPort.status,'open')==1)
           fclose(handles.SerialPort);
       end
       errordlg(e.message); % xu ly loi ngoai le, neu khong co ngoai le xay ra thi se thuc hien catch
    end
        
else
    set(handles.ButtonConnect, 'String','Connect')
    fclose(handles.SerialPort);
end
guidata(hObject, handles); % hObject la cai hien tai

%%% Callback function
function localReadAndPlot(interfaceObject,~,Channel,bytesToRead)
%%% 
load handles;
%%% Declare variables
global dataall;global datac;    %n%umber of data send: count packets/frames
global data1;global data2;global data3;global data4;
global data5;global data6;global data7;global data8;
global L; global x; global y;
%%% Read the desired number of data bytes
data = fread(interfaceObject,bytesToRead); 
dataall=[dataall;data];

%%% Prepare of obtaining new data and plotting
if ~isempty(datac)
    synIndex=length(data1)*33;  %%use length data 1 to retrive corrupt segment of the previous loop
else
    synIndex=1;                 %%if it is the 1st time to get data, synIndex run from the beginning
    L=length(datac);            %%initial L (length of data=[])=0
    nPts=1000;                  %%number of points to plot
    x=ones(1,nPts)*NaN;         %%prepare for x and y
    y=ones(1,nPts)*NaN;
end
% for i=k+1:length(dataall)-32
while (synIndex <(length(dataall)-32))
    if ((dataall(synIndex)==254)&&(dataall(synIndex+1)==1))
        data1=[data1; dataall(synIndex+5)*65536+dataall(synIndex+6)*256+dataall(synIndex+7)];
        data2=[data2; dataall(synIndex+8)*65536+dataall(synIndex+9)*256+dataall(synIndex+10)];
        data3=[data3; dataall(synIndex+11)*65536+dataall(synIndex+12)*256+dataall(synIndex+13)];
        data4=[data4; dataall(synIndex+14)*65536+dataall(synIndex+15)*256+dataall(synIndex+16)];
        data5=[data5; dataall(synIndex+17)*65536+dataall(synIndex+18)*256+dataall(synIndex+19)];
        data6=[data6; dataall(synIndex+20)*65536+dataall(synIndex+21)*256+dataall(synIndex+22)];
        data7=[data7; dataall(synIndex+23)*65536+dataall(synIndex+24)*256+dataall(synIndex+25)];
        data8=[data8; dataall(synIndex+26)*65536+dataall(synIndex+27)*256+dataall(synIndex+28)];
        datac=[datac; dataall(synIndex+29)*(2^24)+dataall(synIndex+30)*65536+dataall(synIndex+31)*256+dataall(synIndex+32)];
        synIndex=synIndex+33;
    else 
        synIndex=synIndex+1;
    end
end
%%% Save new data to workspace
assignin('base','dataall',dataall);assignin('base','datac',datac);
assignin('base','data1',data1);assignin('base','data2',data2);
assignin('base','data3',data3);assignin('base','data4',data4);
assignin('base','data5',data5);assignin('base','data6',data6);
assignin('base','data7',data7);assignin('base','data8',data8);
%%% Select Channel to plot data
switch Channel
    case 1
        dataplot=data1;
    case 2
        dataplot=data2;
    case 3
        dataplot=data3;
    case 4
        dataplot=data4;
    case 5
        dataplot=data5;
    case 6
        dataplot=data6;
    case 7
        dataplot=data7;
    case 8
        dataplot=data8;
end
%%%update data for plotting only
if ~isempty(datac)
    new=length(datac)-L;                                %%new= number of new obtained data
    x(1:end-new)=x(new+1:end);                          %%x shift old data left
    x(end-new+1:end)=datac(end-new+1:end)-datac(1);     %%add new data x
    y(1:end-new)=y(new+1:end);                          %%y shift old data left    
    y(end-new+1:end)=dataplot(end-new+1:end)*4.5/(2^23-1)-4.5;%%add new data y and changing to Voltage
end
L=length(datac);                                        %%update length of new data

%%%add padding about 15% each side of signal min, max
if min(y)>0
    frame=[0.85*min(y) 1.15*max(y)];
else
    if max(y)>0
        frame=[-1.15*abs(min(y)) 1.15*max(y)];
    else
        frame=[-1.15*abs(min(y)) -0.85*abs(max(y))];
    end
end
%%%plot new frame
plot(handles.axes1,x,y);
set(handles.axes1,'XLim',[min(x) max(x)], 'YLim',frame);
assignin('base','x',x);assignin('base','y',y);

% --- Executes on button press in ButttonExit.
function ButttonExit_Callback(hObject, eventdata, handles)
% hObject    handle to ButttonExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;

%%%%%%%%%%%%%%%% SUPPORT FUNCTION [1]
function str = getCurrentPopupString(hh)
%# getCurrentPopupString returns the currently selected string in the popupmenu with handle hh

%# could test input here
if ~ishandle(hh) || strcmp(get(hh,'Type'),'popupmenu')
error('getCurrentPopupString needs a handle to a popupmenu as input')
end

%# get the string - do it the readable way
list = get(hh,'String');
val = get(hh,'Value');
if iscell(list)
   str = list{val};
else
   str = list(val,:);
end

%--- Executes on button press in ButtonSave.
function ButtonSave_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% evalin('base','dataall');
load handles;
global dataall;
uisave('dataall.mat','dataall');


% --- Executes on selection change in ListChan.
function ListChan_Callback(hObject, eventdata, handles)
% hObject    handle to ListChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListChan contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListChan


% --- Executes during object creation, after setting all properties.
function ListChan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

segmentLength = round(numel(data1)/4.5); % Equivalent to setting segmentLength = [] in the next line
figure;
subplot(1,2,1);
data1ft = filter(LP43s,double(data1));
data1ftft = filter(HP03one,data1ft);
spectrogram(data1ftft,round(segmentLength/5),round(70/100*segmentLength/5),[],250,'yaxis');
title('Chan1');
ylim([0 40]);
subplot(1,2,2);
data2ft = filter(LP43s,double(data2));
data2ftft = filter(HP03one,data2ft);
spectrogram(data2ftft,round(segmentLength/5),round(70/100*segmentLength/5),[],250,'yaxis');
title('Chan2');
ylim([0 40]);
datatestvalue = data2;
%figure;
for i=1:250

t1=i*50;
datatest = datatestvalue(t1:(t1+510));
%datatest = data2(1:end);

Fs = 250; % Sampling frequency
T = 1/Fs; % Sample time
L = length(datatest); % Length of signal
t = (0:L-1)*T; % Time vector



NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(datatest,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
% Plot single-sided amplitude spectrum.

y=2*abs(Y(1:NFFT/2+1));
%y=smooth(y);

%Ploting
interestY = y(1:82); %f value from 5 to 40Hz
interestF = f(1:82); %12

plot(interestF,interestY);
title('FFT of 12.5Hz Stimulus - Single-Sided Amplitude Spectrum of y(t) ')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')
xlim([5 40]);


% Detect peak
%
indexmax = find(max(interestY) == interestY); %find index of peak y
xmax = interestF(indexmax);
ymax = interestY(indexmax);

     %annotate

%%SNR
meanY = mean(interestY);
snr = ymax/meanY;

%snr = 20 * log10((ymax/meanY)^2);
strmax = ['Max= ',num2str(xmax),' SNR= ',num2str(snr)];
%strmax = ['Max= ',num2str(xmax)];

textcolor='rbkymg';
%change color based on algo decision
if (snr > 3) && ( 12< xmax) && (xmax <13)
  tc = 1; %assign red if it is a detection  
else
  tc = 2;
end
    
%text(xmax,ymax,strmax,'HorizontalAlignment','left','color',textcolor(tc));  

pause(0.2);
display(t1);
end

%f = 10-164
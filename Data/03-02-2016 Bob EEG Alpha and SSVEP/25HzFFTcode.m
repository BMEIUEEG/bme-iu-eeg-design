
figure;
datatest = data2ftft(7000:10000);
Fs = 250; % Sampling frequency
T = 1/Fs; % Sample time
L = length(datatest); % Length of signal
t = (0:L-1)*T; % Time vector


%%

NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(datatest,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
% Plot single-sided amplitude spectrum.

y=2*abs(Y(1:NFFT/2+1));
y=smooth(y);
plot(f,y)
title('FFT of 25Hz Stimulus - Single-Sided Amplitude Spectrum of y(t) - smoothed')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')
xlim([5 40]);
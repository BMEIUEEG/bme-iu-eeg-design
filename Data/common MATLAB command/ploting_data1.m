title('Eye movement trial: 5s upward; 10s forward; [no filter - shift mean line to 0]');
xlabel('Sample');
ylabel('Volt');
%%
plot(data1);
data1Vol=data1*4.5/(2^23-1)-4.5;
figure;
plot(data1Vol);

data1Volfilter=filter(hp052,data1Vol);
hold on;
plot(data1Volfilter);



%%
data1Volshift=data1Vol-mean(data1Vol);
plot(data1Volshift);



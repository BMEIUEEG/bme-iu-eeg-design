%plot two different data at two SPS on time scale
lowdata = data1_250;
highdata = data1_500;
x = linspace(1,length(lowdata)/250,length(lowdata));
x2 = linspace(1,length(highdata)/(250*2),length(highdata));

figure;
plot(x,lowdata,'-v')
xlim = ([5 10]);
hold on;
plot(x2,highdata,'-rs');

xlabel('Time(s)')
ylabel('Voltage(V)')

%% Impedance calculation
%by 250SPS
seglow = lowdata(1000:1500);
imp1low = (max(seglow) - min(seglow))/2;
implow = imp1low/(6e-9) 

%by 500SPS
seghigh = highdata(1000:2000);
imp1high = (max(seghigh) - min(seghigh))/2;
imphigh = imp1high/(6e-9)

%by filter out the 31Hz
segfilter = filter(Hd_BPass_27_35,lowdata);
segfilter = segfilter(1000:1500);
impfilter = (max(segfilter) - min(segfilter))/2;
impfilter = impfilter/(6e-9)

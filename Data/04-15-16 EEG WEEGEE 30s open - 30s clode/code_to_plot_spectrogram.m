    load('D:\Repo\bme-iu-eeg-design\Data\Generic parameter\eegfilter2.mat');
 
    %data1 = data1(startdata:end);
    %data2 = data2 (startdata:end);
    %data3 = data3(startdata:end);
    
    startgraph = 10000;
    segmentLength = round(numel(data1(startgraph:end))/4.5); % Equivalent to setting segmentLength = [] in the next line
    figure;
    subplot(2,1,1);
    data1ft = filter(LP43s,double(data1));
    data1ftft = filter(HP03one,data1ft);
    spectrogram(data1ftft(startgraph:end),round(segmentLength/6),round(80/100*segmentLength/6),[],250,'yaxis');
    title('Chan1');
    ylim([4 22]);
    %xlim([10 100]);
 
    
    subplot(2,1,2);
    data2ft = filter(LP43s,double(data2));
    data2ftft = filter(HP03one,data2ft);
    spectrogram(data2ftft(startgraph:end),round(segmentLength/6),round(80/100*segmentLength/6),[],250,'yaxis'); %front of yxis
    title('Channel 2 ')
    ylim([4 22]);
% 
%     subplot(2,2,3);
%     data3ft = filter(LP43s,double(data3));
%     data3ftft = filter(HP03one,data3ft);
%     spectrogram(data3ftft(startgraph:end),round(segmentLength/6),round(80/100*segmentLength/6),[],250,'yaxis');
%     title('Chan3');
%     ylim([5 35]);
% %%generate code for offline review
% ALLEEG.data = zeros(3, size(data1ftft,2));
% ALLEEG.data(1,:) = data1ftft;
% ALLEEG.data(2,:) = data2ftft;
% ALLEEG.data(3,:) = data3ftft;
% %ALLEEG.event = MarkerStruct;
% ALLEEG.srate = 250;
% for i=1:length(MarkerStruct)
%     ALLEEG.event(i).latency = MarkerStruct(i).Latency;
%     ALLEEG.event(i).type = MarkerStruct(i).Note;
% end
    

    

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include <stm32f4xx.h>
#include <stm32f4xx_conf.h>
#include <stm32f4xx.h>
#include <misc.h>			 // I recommend you have a look at these in the ST firmware folder
#include <stm32f4xx_usart.h> // under Libraries/STM32F4xx_StdPeriph_Driver/inc and src



int main(void) {
///////// Initial Portion /////////////////////////
  init_USART1(9600); // initialize USART1 @ 9600 baud
	SystemCoreClockUpdate();                      /* Get Core Clock Frequency   */
  if (SysTick_Config(SystemCoreClock / 1000)) { /* SysTick 1 msec interrupts  */
    while (1);                                  /* Capture error              */
  }
  USART_puts(USART1, "Init 1 complete! Hello World!rn"); // just send a message to indicate that it works

		//Init GPIO for Leds on board and button input
	init_GPIO();
	
	/////////////////- Loop - ////////////////////////////////////
  while (1){
    
     // You can do whatever you want in here
		USART_puts(USART1, "Init 2 complete! Hello World! \r \n"); // just send a message to indicate that it works
				
		//GPIO_ToggleBits(GPIOD, GPIO_Pin_15 | GPIO_Pin_14 | GPIO_Pin_13| GPIO_Pin_12 );
		GPIO_ToggleBits(GPIOD, GPIO_Pin_12 ); //Test Leds on board
		Delay(200);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_13 ); //Test Leds on board
		Delay(200);
		GPIO_ToggleBits(GPIOD, GPIO_Pin_14 ); //Test Leds on board
		Delay(1000);
     
  }
//////////////////////////////////////////////////////////	
}




#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif

/**
  * @}
  */


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

% IIR Realtime filter
% M ~ denominator orders
% L ~ numerator orders
% a ~ numerator coefficient
% b ~ denominator coefficient
% w ~ internal state of denominator delay register
% v ~ internal statet of numerator delay register
% x ~ input signal
function [yr,wr,vr]=FilterRealtimeIIR(M,a,L,b,w,v,x)

    v(1)=x;                  %current input sample
    w(1)=0;                  %current output to be compared
    for i=1:L+1
        w(1)=w(1)+b(i)*v(i); %numerator part    
    end
    for i=2:M+1
        w(1)=w(1)-a(i)*w(i); %denominator  part    
    end
    for i=L+1:-1:2
        v(i)=v(i-1);         %reverse-order updating of v
    end
    for i=M+1:-1:2
        w(i)=w(i-1);         %reverse-order updating of w
    end
    yr=w(1);                 %current output sample
    wr=w;
    vr=v;
end
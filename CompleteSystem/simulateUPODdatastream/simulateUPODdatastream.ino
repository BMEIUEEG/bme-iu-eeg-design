/*
  Analog input, analog output, serial output

 Reads an analog input pin, maps the result to a range from 0 to 255
 and uses the result to set the pulsewidth modulation (PWM) of an output pin.
 Also prints the results to the serial monitor.

 The circuit:
 * potentiometer connected to analog pin 0.
   Center pin of the potentiometer goes to the analog pin.
   side pins of the potentiometer go to +5V and ground
 * LED connected from digital pin 9 to ground

 created 29 Dec. 2008
 modified 9 Apr 2012
 by Tom Igoe

 This example code is in the public domain.

 */

// These constants won't change.  They're used to give names
// to the pins used:
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogOutPin = 9; // Analog output pin that the LED is attached to

int sensorValue = 0;        // value read from the pot
int outputValue = 0;        // value output to the PWM (analog out)

int mox1 = 200;
int mox2 = 32;
int e2v1 = 146;
int e2v2 = 133;
int filler = 1;

char tab[]=",";
char slash[]=".";
char colon[]=":";
char nan[]="NaN";



void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);

}

void loop() {
  // read the analog in value:

Serial.print("UPODDE"); //model name
Serial.print(tab);
Serial.print(2016); //current year
Serial.print(slash);
Serial.print(8); //current month
Serial.print(slash);
Serial.print(29); //current day
Serial.print(tab);
Serial.print(2); //curent hr
Serial.print(colon);
Serial.print(14); //current minute
Serial.print(colon);
Serial.print(12); //current second
Serial.print(tab);
Serial.print(1472437412); //current unixtime; time since midnight 1/1/1970 (seconds)
Serial.print(tab);
Serial.print(analogRead(A1)); //Baseline VOC sensor
Serial.print(tab);
Serial.print(analogRead(A2)); //CO2 sensor
Serial.print(tab);
Serial.print(analogRead(A0)); //analog channel 0
Serial.print(tab);
Serial.print(mox1); //Figaro 1
mox1++;
Serial.print(tab);
Serial.print(mox2); //Figaro 2
Serial.print(tab);
Serial.print(e2v1); //e2v_O3
Serial.print(tab);
Serial.print(e2v2); //e2v_NO2
Serial.print(tab);
Serial.print(filler); //e2v_1
Serial.print(tab);
Serial.print(filler); //e2v_2
Serial.print(tab);
Serial.print(filler); //e2v_3
Serial.print(tab);
Serial.print(filler); //e2v_4
Serial.print(tab);
Serial.print(24.6); //RTH3
Serial.print(tab);
Serial.print(29.6); //RHT humi
Serial.print(tab);
Serial.print(filler); //e2v_4
Serial.print(tab);
Serial.print(filler); //e2v_4
Serial.print(tab);
Serial.print(filler); //e2v_4


Serial.println();
delay(5000);
}

/*
 * Update: integrate Wifi
 */

#include <vector>
#include <string>
#include <ESP8266WiFi.h>

using namespace std;

//===========================================
const char* ssid     = "TanSinh-N1";
const char* password = "tansinh2015";
String writeAPIKey   = "IL2UE2WIXGAQ4FB3";

const char* thingSpeakAddress = "api.thingspeak.com";
const int updateThingSpeakInterval = 15*1000;

long lastConnectionTime = 0;
boolean lastConnected = false;
int failedCounter=0;

//============================================
vector<String> arr;
String inputString = "";  // string to hold new data
bool stringComplete = false;

//============================================
vector<String> split(String str, String sep){
    char* cstr = const_cast<char*>(str.c_str());
    char* current;
    vector<String> arr;
    current = strtok(cstr, sep.c_str());
    while (current != NULL) {
        arr.push_back(current);
        current = strtok(NULL, sep.c_str());
    }
    return arr;
}

// ==================================
void updateThingSpeak(String tsData)
{
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(thingSpeakAddress, httpPort)) {
    failedCounter++;
    Serial.println("Connection to ThingSpeak Failed ("+String(failedCounter, DEC)+")");   
    Serial.println();
    
    lastConnectionTime = millis(); 
    return;
  }
  client.print("POST /update HTTP/1.1\n");
  client.print("Host: api.thingspeak.com\n");
  client.print("Connection: close\n");
  client.print("X-THINGSPEAKAPIKEY: "+writeAPIKey+"\n");
  client.print("Content-Type: application/x-www-form-urlencoded\n");
  client.print("Content-Length: ");
  client.print(tsData.length());
  client.print("\n\n");

  client.print(tsData);
    
  lastConnectionTime = millis();
    
  if (client.connected())
  {
      delay(10);
      Serial.println("Connecting to ThingSpeak...");
      Serial.println();
      
      failedCounter = 0;
      while(client.available()){
        String line = client.readStringUntil('\r');
        Serial.print(line);
      }
  
      Serial.println();
      Serial.println("closing connection");
  }
  else
  {
      failedCounter++;
  
      Serial.println("Connection to ThingSpeak failed ("+String(failedCounter, DEC)+")");   
      Serial.println();
  }
}


/* ================================================*/
void setup(){
  // ================= Setup serial =================
  Serial.begin(9600);
  delay(10);
  
  inputString.reserve(200);
  Serial.println("Hello!!!");
 
  // ========= Connecting to a WiFi network =========
  Serial.print("\n\nConnecting to ");
  Serial.println(ssid); 
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi connected. IP address: "+WiFi.localIP());
}

void loop(){
  // ======== Merging string ====================
  while (Serial.available()) {
    // get new byte
    char inChar = (char)Serial.read();
    
    // add it to the inputString
    inputString += inChar;
    
    // if newline --> set flag
    if (inChar == '\n') {
      stringComplete = true;
    }
  }

  // ================== Split string ====================
  if (stringComplete) {
    Serial.println(inputString.c_str());    // print the whole string, just to check
    arr = split(inputString, ",");  
     

    // Print all elements of split string
    for(size_t i=0; i<arr.size(); i++){
        //printf("%s\n", arr[i].c_str());
        Serial.println(arr[i].c_str());
    }
 
    
    // =========== Clear the string
    inputString = "";
    stringComplete = false;

    // ========== Sending to server (ThingSpeak) ================
    if ((WiFi.status() == WL_CONNECTED) && 
        (millis() - lastConnectionTime > updateThingSpeakInterval))
    {
      Serial.println("Sending data to ThinkSpeak, please wait...");

      // ========== Print some sensors' info - for debugging only =====================
      //Serial.print("Size: ");     Serial.println(arr.size());   // check size of the output string
      //Serial.print("max_size: "); Serial.println(arr.max_size());
      //Serial.print("Figaro MOx sensor#1: ");  Serial.println(arr[7].c_str());  
      //Serial.print("RHT03 Temperature: ");    Serial.println(arr[15].c_str()); 
      
      //updateThingSpeak("field4=" + arr[7] + "&field5=" + arr[15]);
      updateThingSpeak("field5=" + arr[7]);
       
      // Serial.println("Done.");
    }
  }
}

clear all; close all; clc;

%% 1.Specify COM Port that arduino is connected to.
comPort = '/dev/cu.usbserial-DA01LLCR';
TIME_REFRESH = 20;  % time to wait before the new data sent

%% 2. Intializes the Serial Port -- setupSerial()
if (~exist('serialFlag', 'var'))
    [upod.s,serialFlag] = setupSerial(comPort);
end

%% 3. Open a new figure - add start stop and close serial buttons
%intializes the figure we will plot in if it does not exist
if(~exist('h','var') || ~ishandle(h))
    h = figure(1);
end
%add a start/stop and close serial button inside the figure
%This button calls the 'stop_call_tempGraph' function everytime it is
%pressed
if(~exist('button', 'var'))
    button = uicontrol('Style', 'pushbutton', 'String', 'Stop',...
        'pos', [0 0 50 25], 'parent',h,...
        'Callback', 'stop_call_tempGraph','UserData',1);
end

%This button calls the 'closeSerial' function everytime it is pressed
if(~exist('button2','var'))
    button2 = uicontrol('Style','pushbutton','String', 'Close Serial Port',...
        'pos',[370 0 150 25], 'parent',h,...
        'Callback','closeSerial','UserData',1);
end


%% 4. Create the surface graphics object
% Intializes the axis if they do not exist
if(~exist('myAxes', 'var'))
    % Initializes plotting vectors
    buf_len = 60; % 1200 ~ 20 min; 3000 ~ 45 min.; 60 ~ 10 min. of data
    index = 1:buf_len;
    zeroIndex = zeros(size(index));
    fig1data = zeroIndex;
    fig2data = zeroIndex;

    limits = [0 500]; %expected range of CO2 for plotting and color scaling
    limits1 = [0 500]; %Expected range of VOCs

    % Set the axes and select a 2D (x,z) view
    myAxes = axes('position', [0.1 0.35 0.35 0.25], 'XLim',[0 buf_len],'YLim',[0 0.1],'Zlim', limits,'CLim',limits);
    view(0,0);
    grid on;

    % Create surface graphics object s to display temperature data in (x,z)
    % plane, area color based on temperature readings
    %           'XData''YData'  'ZData'             'CData'
    s = surface(index, [0, 0], [fig1data; zeroIndex], [fig1data; fig1data]);

    % Set surface face and edges to fill with CData values, add labels
    set(s, 'EdgeColor', 'flat', 'FaceColor', 'flat', 'Parent', myAxes)
    colorbar
    title('Figaro 1, VOCS')
    xlabel('Samples')
    zlabel('ADC Value from Fig1 Sensor')

    myAxes2 = axes('position', [0.55 0.35 0.35 0.25], 'XLim',[0 buf_len],'YLim',[0 0.1],'Zlim', limits1,'CLim',limits1);
    view(0,0);
    grid on;

    s1 = surface(index, [0, 0], [fig2data; zeroIndex], [fig2data; fig2data]);
    set(s1, 'EdgeColor', 'flat', 'FaceColor', 'flat', 'Parent', myAxes2)
    colorbar
    title('Figaro 2, VOCs')
    xlabel('Samples')
    zlabel('ADC Value from fig2 Sensor')

    set(h, 'Position', [200 200 890 660])
    drawnow;

end

%% 5. Runs a loop that continually reads the sensor values
% The temp sensor data is placed in the variable tc.
channelID = 125952;
writeKey  = '1VX93UJOJP55X0DJ';

% Write 10 values to each field of your channel along with timestamps
mode = 'R';
tlast = now;    %save current system time
while(get(button, 'UserData'))
     fprintf(upod.s, mode);
     [fig1,fig2] = readUPODdata(upod);

     fig1 = [fig1];
     fig2 = [fig2];

% Create table
    %update rolling vector. Append the new reading to the end of the
    %rolling vector data. Drop the first value
    fig1data = [fig1data(2:end), fig1];
    fig2data = [fig2data(2:end), fig2];

    %update plot data and corresponding color
    set(s1,'Zdata',[fig1data; zeroIndex], 'Cdata', [fig1data; fig1data])
    drawnow;
    set(s,'Zdata',[fig2data; zeroIndex], 'Cdata', [fig2data; fig2data])
    %force MATLAB to redraw the figure

    %Plot every TIME_REFRESH interval
    tcurrent = now;
    telapse = (tcurrent - tlast)*24*60*60;
    if ( telapse > TIME_REFRESH )
    % Write data to thingSpeak
    thingSpeakWrite(channelID, [fig1 fig2],'WriteKey',writeKey)

      tlast = tcurrent;
      disp('Data written');
    end
    drawnow;
end

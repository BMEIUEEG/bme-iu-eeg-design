#include "SPI.h"
///SPI Function to Get Data from SPI
void mySPI_SendCommand(uint8_t data, uint8_t delaytime)        // write data to location address
{				

	GPIO_ResetBits(GPIOD, GPIO_Pin_10);											//CS of SPI1: turn on SPI1

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, data);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);
  
	Delay(delaytime);
	GPIO_SetBits(GPIOD, GPIO_Pin_10);												//CS of SPI1: turn off SPI1
}

///SPI function to Send Data
void mySPI_SendData(uint8_t address, uint8_t data)        // write data to location address
{				

	GPIO_ResetBits(GPIOD, GPIO_Pin_10);											//CS of SPI1: turn on SPI1

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
	SPI_I2S_SendData(SPI1, address);
	
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check receive flag, is data recieved?
	SPI_I2S_ReceiveData(SPI1);

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, data);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);

	GPIO_SetBits(GPIOD, GPIO_Pin_10);												//CS of SPI1: turn off SPI1
	
}
////mySPI_GetData
uint16_t mySPI_GetData(uint8_t address)
{									// put in an address; get data from that address

	
	GPIO_ResetBits(GPIOD, GPIO_Pin_10); 											//CS of SPI1: enable ADS - /CS is connected to PC5

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
	SPI_I2S_SendData(SPI1, address);												//send data through SPI1, data: address
	
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
	SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //check transmit flag, is buffer empty?
	SPI_I2S_SendData(SPI1, 0x00);														//Dummy byte to generate clock
	
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //check recieve Flag, is data received?
	SPI_I2S_ReceiveData(SPI1);															//Recieve data from SPI1 (accelerometer)
	
	GPIO_SetBits(GPIOD, GPIO_Pin_10); 												//CS line up; Disable slave???
	
	return SPI_I2S_ReceiveData(SPI1); 											//return reveiced dat
}

///SPI Initialize
void mySPI_Init(void)
{
	GPIO_InitTypeDef GPIO_InitTypeDefStruct; //GPIO structs for Block Control 
	SPI_InitTypeDef SPI_InitTypeDefStruct;   //SPI Struct for SPI1
	//EXTI_InitTypeDef   EXTI_InitStructure;			//EXTI Struct for /DDRY
	//NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE); 	//Enable RCC for SPI1
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); //Enable RCC for GPIOA, GPIOB and GPIOC
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE); //PA5:SCK, PA6: MISO, PA7: MOSI 
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE); //PB0: /RESET, PB1: START, PB2: /PWDN
																											  //PC4: /DRDY, PC5: SPI1-/CS
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	
	//Innitialize SPIx structure
	SPI_InitTypeDefStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16; //Prescaler 16: 42Mhz/16 = 2.625MHz
	SPI_InitTypeDefStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex; //Full Duplex
	SPI_InitTypeDefStruct.SPI_Mode = SPI_Mode_Master;			//Our Microcontroller SMT32 is Master
	SPI_InitTypeDefStruct.SPI_DataSize = SPI_DataSize_8b; //8bit mode
	SPI_InitTypeDefStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
	//Chip select :Software
	//SPI1->CR2 = SPI_CR2_SSOE; May need to enable this bit too to make spi work	
	SPI_InitTypeDefStruct.SPI_FirstBit = SPI_FirstBit_MSB; //Firstbit is MSB: ADS1299 datasheet
	SPI_InitTypeDefStruct.SPI_CPOL = SPI_CPOL_Low;				 //Clock polarity: CPOL=0, clock is low when idle
	SPI_InitTypeDefStruct.SPI_CPHA = SPI_CPHA_2Edge;			 // Defines the edge for bit capture: CPHA= 1 data is sampled at the 2nd edge
	SPI_Init(SPI1, &SPI_InitTypeDefStruct);	
	
	/* configure pins used to control SPI
	 * PA5 = SCK :SP1 Clock
	 * PA6 = MISO:DOUT
	 * PA7 = MOSI:DIN
	 */	
	//Initialize GPIOx Structure for SPI core pins
	GPIO_InitTypeDefStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
	GPIO_InitTypeDefStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitTypeDefStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitTypeDefStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitTypeDefStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitTypeDefStruct);
  
	//Define AF function for each SPI pin
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

 /*Initialize GPIOC for 
 * /CS = PC5
 */ 
	GPIO_InitTypeDefStruct.GPIO_Pin = GPIO_Pin_10; // /CS
	GPIO_InitTypeDefStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitTypeDefStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitTypeDefStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitTypeDefStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOD, &GPIO_InitTypeDefStruct);

	/*Initialize GPIOB and GPIOC for 
	* /RESET = PB0 => PD2
	* START = PB1 => PD0
	* /PWDN = PB2 => N/A
	* /DRDY = PC4 => PD1
	*/	
	GPIO_InitTypeDefStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_2;
	GPIO_InitTypeDefStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitTypeDefStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitTypeDefStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitTypeDefStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOD, &GPIO_InitTypeDefStruct);

	GPIO_InitTypeDefStruct.GPIO_Pin = GPIO_Pin_1; // /DRDY
	GPIO_InitTypeDefStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitTypeDefStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitTypeDefStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitTypeDefStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOD, &GPIO_InitTypeDefStruct);
	
	
	/*	
	// Configure EXTI Line1 
  EXTI_InitStructure.EXTI_Line = EXTI_Line1;
  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;  
  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  EXTI_Init(&EXTI_InitStructure);


	  //Enable and set EXTI Line1 Interrupt to the lowest priority 
  NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);
	*/
	
	//Deactive all Pin
	// Set CS line to high => Disable slave for now
	GPIO_SetBits(GPIOD, GPIO_Pin_10);  //CS
	GPIO_SetBits(GPIOD, GPIO_Pin_2);  //RESET
	GPIO_ResetBits(GPIOD, GPIO_Pin_0); //START
	//GPIO_SetBits(GPIOB, GPIO_Pin_2);	// NO PWDN IN BOB HW
	
	//Enable GPIO1 control register	
	SPI_Cmd(SPI1, ENABLE);
  // SPI_CR1_BR_0 SPI_CR1_SSM  SPI_CR1_BR SPI_CR2_SSOE
}

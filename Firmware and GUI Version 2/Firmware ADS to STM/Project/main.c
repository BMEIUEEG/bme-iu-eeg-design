#include <stm32f4xx.h>
#include <misc.h>
#include "Usart.h"
#include "SysTick.h"
#include "Leds.h"
#include "SPI.h"
#include "ADS_Command.h"
#include <string.h>
#include "stdio.h"
	uint8_t countbytes, countbits,data[10][10],i;
	uint32_t dataChn[10];
	//init for fake package test
	uint8_t countbytes,countbits;
	//uint32_t data_raw[10][10];
	uint32_t dataChn[10];
	uint32_t countall=0;
	
	
	//CHANGE PIN TO MATCH WITH BOB HARDWARE
int main(void) 
{
  char counter;
  Init_USART(384000); // initialize USART1 @ 9600 baud
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	Led_Init();
	InitialiseSysTick();
	/* main program is commmented out for testing
	mySPI_Init();
	
	PowerUp_ADS(); 						// Power up ADS
	SendADSCommand(0x11,2);		// Send Command SDATA: STOP RDATAC mode
	Recognize_ADS();
	
	WREG_ADS(0x01,0xD6);			//Config1: Daisy Chain Mode, Oscillator output disable, DT 250SPS
	WREG_ADS(0x02,0xC0);			//Config2: Test Signals are driven Externally, Test Signal and Amplitude are Default
	WREG_ADS(0x03,0xE1);			//Config3: Unsing Internal Reference
	WREG_ADS(0x05,0x00);			//Channel 1-8: Normal Operation;PGA=1;SRB2 off;Normal electrode input
	WREG_ADS(0x06,0x00);			//-
	WREG_ADS(0x07,0x00);			//-
	WREG_ADS(0x08,0x00);			//-3
	WREG_ADS(0x09,0x00);			//-
	WREG_ADS(0x0A,0x00);			//-
	WREG_ADS(0x0B,0x00);			//-
	WREG_ADS(0x0C,0x00);			//-8
	
	*/

/* Square wave test signal 

	WREG_ADS(0x02,0xD0);			//
	WREG_ADS(0x05,0x05);
	WREG_ADS(0x06,0x05);
	WREG_ADS(0x07,0x05);
	WREG_ADS(0x08,0x05);
	WREG_ADS(0x09,0x05);
	WREG_ADS(0x0A,0x05);
	WREG_ADS(0x0B,0x05);
	WREG_ADS(0x0C,0x05);
*/
	/* main programe
	
	GPIO_SetBits(GPIOD, GPIO_Pin_0); //Enable START, then /DRDY toggle at f_CLK/8192 =>t=4ms
	Delay(10);	
	
	GetData_RDATAC();
	*/
	
	//Test static fake value


		counter =0;
		
	//init value for the array of a fake frame
	for (countbytes=1;countbytes<10;countbytes++)
	{
		for (countbits=1;countbits<4;countbits++)
		{
			data[countbytes][countbits]=0x10+counter;
			
		}
	}
//send frame to serial port or MATLAB
	while(1)
	{
			for (countbytes=1;countbytes<10;countbytes++)
	{
		for (countbits=1;countbits<4;countbits++)
		{
			data[countbytes][countbits]=counter*countbytes;
			//counter++;
		}
	}
	counter++;
	
	//start sending fake data
		Delay(1000);
		Send_Header(0xfe,0x01);
		Send_Header(0xfe,0x01);
		Send_Header(0xfe,0x01);
 		
	for (countbytes=1;countbytes<10;countbytes++)
	{			
		dataChn[countbytes]=0;
		for (countbits=1;countbits<4;countbits++)
		{
			dataChn[countbytes]=dataChn[countbytes]|(data[countbytes][countbits]<<((3-countbits)*8));
		}
		if(countbytes!=1)
		{
		if (dataChn[countbytes]<0x00800000)
			{
				dataChn[countbytes]=dataChn[countbytes]+0x00800000;
			}
			else
			{
				dataChn[countbytes]=dataChn[countbytes]-0x00800000;
			}
		}
		Send_Data_uint24(dataChn[countbytes]);
	}
	countall++; //count number of data send
	Send_Data_uint32(countall);
	//USART_puts(USART1, "\n" ); % no more terminator
	}
	//



/*
	while(1)
	{	
		//Send_Header(0xfe,0x01);
		GetData_RDATAC();
		//GetData_RDATA();							//Retrive Data from RDATA mode
		
	}
*/
}
 

close all;
% clear all;
% clc;
fs=250; %sampling frequency
y=linebuffer_x;
L=length(y);
yff=fft(y,L); %Fourier transform
fy=yff.*conj(yff)/L; %magnitude spectrum
fx=fs*(0:L/2)/L; %frequency axis
plot(fx,fy(1:length(fx)));title('FFT y'); %plot frequency domain of the signal
axis([2 32 0 10^-6]);

f = fdesign.notch('N,F0,Q',10,50,10,250);
h = design(f);
yf=filter(h,y);
Lf=length(yf);
yfff=fft(yf,Lf); %Fourier transform
fyf=yfff.*conj(yfff)/Lf; %magnitude spectrum
fxf=fs*(0:Lf/2)/Lf; %frequency axis
figure;
plot(fxf,fyf(1:length(fxf)));title('FFT yf');
axis([2 32 0 10^-6]);
% Function Filter FIR in realtime
% x ~ input signal
% y ~ output signal
% w ~ current content of delay register
% M ~ th filter order
function [yr, wr] = FilterRealtimeFIR(x,h,M,w)
    y=0;                % initialise value y at the beginning
    w(1)=x;             % assign the current state
    for i=1:M+1
        y=y+h(i)*w(i);  % compute convolution of filter window and signal for output y
    end
    for i=M+1:-1:2
        w(i)=w(i-1);    % shift delay register for next time instant
    end
    yr=y;               %y result
    wr=w;               %w result
end
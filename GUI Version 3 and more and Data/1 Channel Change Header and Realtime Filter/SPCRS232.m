function varargout = SPCRS232(varargin)
% SPCRS232 M-file for SPCRS232.fig
%      SPCRS232, by itself, creates a new SPCRS232 or raises the existing
%      singleton*.
%
%      H = SPCRS232 returns the handle to a new SPCRS232 or the handle to
%      the existing singleton*.
%
%      SPCRS232('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPCRS232.M with the given input arguments.
%
%      SPCRS232('Property','Value',...) creates a new SPCRS232 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SPCRS232_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SPCRS232_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SPCRS232

% Last Modified by GUIDE v2.5 22-Jun-2014 15:17:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SPCRS232_OpeningFcn, ...
                   'gui_OutputFcn',  @SPCRS232_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SPCRS232 is made visible.
function SPCRS232_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SPCRS232 (see VARARGIN)

% Choose default command line output for SPCRS232
handles.output = hObject;
delete(instrfindall);      % Reset Comport

% Update handles structure
guidata(hObject, handles);
save('handles.mat', 'handles');

% UIWAIT makes SPCRS232 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SPCRS232_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in ListPortname.
function ListPortname_Callback(hObject, eventdata, handles)
% hObject    handle to ListPortname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListPortname contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListPortname
% PortNumber=get(handles.ListPortname,'Value');
% Portname=['COM',num2str(PortNumber)];
%set(handles.TextNumber,'String',str);

% --- Executes during object creation, after setting all properties.
function ListPortname_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListPortname (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListBaudrate.
function ListBaudrate_Callback(hObject, eventdata, handles)
% hObject    handle to ListBaudrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListBaudrate contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListBaudrate


% --- Executes during object creation, after setting all properties.
function ListBaudrate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListBaudrate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListDatabits.
function ListDatabits_Callback(hObject, eventdata, handles)
% hObject    handle to ListDatabits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListDatabits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListDatabits

% --- Executes during object creation, after setting all properties.
function ListDatabits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListDatabits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListParity.
function ListParity_Callback(hObject, eventdata, handles)
% hObject    handle to ListParity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListParity contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListParity

% --- Executes during object creation, after setting all properties.
function ListParity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListParity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in ListStopbits.
function ListStopbits_Callback(hObject, eventdata, handles)
% hObject    handle to ListStopbits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListStopbits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListStopbits


% --- Executes during object creation, after setting all properties.
function ListStopbits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListStopbits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
guidata(hObject, handles);

% --- Executes on button press in ButtonConnect.
function ButtonConnect_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonConnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)Portname =
% getCurrentPopupSjhtring(handles.ListPortname);
% SerialPort=serial(getCurrentPopupString(ListPortname));
global dataall;dataall=[];global datac;datac=[];
global data1;data1=[];global data2;data2=[];
global data3;data3=[];global data4;data4=[];
global data5;data5=[];global data6;data6=[];
global data7;data7=[];global data8;data8=[];
global dataplot; dataplot=[];
% set(handles.TextPortdata,'String',getCurrentPopupString(ListPortname));
a=get(handles.ButtonConnect,'String');
if strcmp(a,'Connect')
    assignin('base', 't0', 0); % tao trong workspace bien t0=0 de chuan bi lam 1 data moi
    Portname=getCurrentPopupString(handles.ListPortname);
    SerialPort=serial(Portname);
    SerialPort.Baudrate=str2num(getCurrentPopupString(handles.ListBaudrate));
    SerialPort.Databits=str2num(getCurrentPopupString(handles.ListDatabits));
    SerialPort.Parity=getCurrentPopupString(handles.ListParity);
    SerialPort.Stopbits=get(handles.ListStopbits,'Value');
    SerialPort.Baudrate=str2num(getCurrentPopupString(handles.ListBaudrate));
    SerialPort.InputBufferSize=500000;
    SerialPort.BytesAvailableFcnCount = 300;
%   SerialPort.BytesAvailableFcnMode = 'terminator';
    SerialPort.BytesAvailableFcnMode = 'byte';
        
    Channel=str2num(getCurrentPopupString(handles.ListChan));
    hold on;
    plotHandle1=plot(handles.axes1,0,'-b','LineWidth',1);    
    axis(handles.axes1,[0 1000 -3 3]);
    SerialPort.BytesAvailableFcn = {@localReadAndPlot,plotHandle1,Channel,300};
    try
       handles.SerialPort = SerialPort; % s chinh la handles.s 
       fopen(handles.SerialPort);
       % hien thi Disconnect
       set(handles.ButtonConnect, 'String','Disconnect')
       drawnow;
    catch e
       if(strcmp(handles.SerialPort.status,'open')==1)
           fclose(handles.SerialPort);
       end
       errordlg(e.message); % xu ly loi ngoai le, neu khong co ngoai le xay ra thi se thuc hien catch
    end
        
else
    set(handles.ButtonConnect, 'String','Connect')
    fclose(handles.SerialPort);
end
guidata(hObject, handles); % hObject la cai hien tai

function localReadAndPlot(interfaceObject,~,figureHandle1,Channel,bytesToRead)
% % 
load handles;
global h;global dataall;global datac;    %number of data send: count packets/frames
global data1;global data2;global data3;global data4;
global data5;global data6;global data7;global data8;
persistent  wDataCH1;persistent  vDataCH1;     % for filter
global  dataplot; global Type; global af, global bf;
% Read the desired number of data bytes
data = fread(interfaceObject,bytesToRead); 
dataall=[dataall;data];
if ~isempty(data1)
    synIndex=length(data1)*37;  %use length data 1 to retrive corrupt segment of the previous loop
else
    synIndex=1;
    switch Type
        case 1
            wDataCH1=zeros(1,length(h));   % initialize w CH1
        case 2
            wDataCH1=zeros(1,length(af));   % initialize w CH1
            vDataCH1=zeros(1,length(bf));
    end
end
data1n=[];data2n=[];data3n=[];data4n=[];data5n=[];data6n=[];data7n=[];data8n=[];datacn=[];
while (synIndex <(length(dataall)-38))
    if ((dataall(synIndex)==254)&&(dataall(synIndex+1)==254)&&(dataall(synIndex+2)==254)&&(dataall(synIndex+3)==1)&&(dataall(synIndex+4)==1)&&(dataall(synIndex+5)==1)&&(dataall(synIndex+37)==254)&&(dataall(synIndex+38)==254))
        data1n=[data1n; dataall(synIndex+9)*65536+dataall(synIndex+10)*256+dataall(synIndex+11)];
        data2n=[data2n; dataall(synIndex+12)*65536+dataall(synIndex+13)*256+dataall(synIndex+14)];
        data3n=[data3n; dataall(synIndex+15)*65536+dataall(synIndex+16)*256+dataall(synIndex+17)];
        data4n=[data4n; dataall(synIndex+18)*65536+dataall(synIndex+19)*256+dataall(synIndex+20)];
        data5n=[data5n; dataall(synIndex+21)*65536+dataall(synIndex+22)*256+dataall(synIndex+23)];
        data6n=[data6n; dataall(synIndex+24)*65536+dataall(synIndex+25)*256+dataall(synIndex+26)];
        data7n=[data7n; dataall(synIndex+27)*65536+dataall(synIndex+28)*256+dataall(synIndex+29)];
        data8n=[data8n; dataall(synIndex+30)*65536+dataall(synIndex+31)*256+dataall(synIndex+32)];
        datacn=[datacn; dataall(synIndex+33)*(2^24)+dataall(synIndex+34)*65536+dataall(synIndex+35)*256+dataall(synIndex+36)];
        synIndex=synIndex+37;
    else 
        synIndex=synIndex+1;
    end
end
data1=[data1; data1n];data2=[data2; data2n];data3=[data3; data3n];data4=[data4; data4n];
data5=[data5; data5n];data6=[data6; data6n];data7=[data7; data7n];data8=[data8; data8n];
datac=[datac; datacn];
assignin('base','dataall',dataall);assignin('base','datac',datac);
assignin('base','data1',data1);assignin('base','data2',data2);
assignin('base','data3',data3);assignin('base','data4',data4);
assignin('base','data5',data5);assignin('base','data6',data6);
assignin('base','data7',data7);assignin('base','data8',data8);

% %%% Select Channel to plot data
switch Channel
    case 1
        datain=data1n;
    case 2
        datain=data2n;
    case 3
        datain=data3n;
    case 4
        datain=data4n;
    case 5
        datain=data5n;
    case 6
        datain=data6n;
    case 7
        datain=data7n;
    case 8
        datain=data8n;
end
lenData=length(datain);
switch Type
    case 1
        % %-----------------filter FIR----------------------------------------------
        M0 = length(h)-1;   %M0 order
        %=======================filter channel 1==================================
        nDataCH1f0=zeros(1,lenData);
        for i=1:1:lenData
            [nDataCH1f0(i),wDataCH1] = FilterRealtimeFIR(datain(i)*4.5/(2^23-1)-4.5,h,M0,wDataCH1);
        end
    case 2
        M0=length(af)-1;
        L0=length(bf)-1;
        nDataCH1f0=zeros(1,lenData);
        for i=1:1:lenData
            [nDataCH1f0(i),wDataCH1,vDataCH1]= FilterRealtimeIIR(M0,af,L0,bf,wDataCH1,vDataCH1,datain(i)*4.5/(2^23-1)-4.5);
        end
        assignin('base','w',wDataCH1);
        assignin('base','v',vDataCH1);
end


dataplot=[dataplot nDataCH1f0];
if (length(dataplot)>1000)
    y1=dataplot(end-1000:end);
    %x=datac(end-500:end);
else
    y1=dataplot;
    %x=(1:length(y1));
end
%save('mydata',data(x));
assignin('base','dataplot',dataplot);
% Update the plot
axis(handles.axes1,[0 1000 min(y1) max(y1)]);
set(figureHandle1,'Ydata',y1);%,'Xdata',x);
  
% axis(handles.axes1,[0 500 -10 10]);
%      drawnow                                 % refresh display
%      % refreshdata(handles.axes1);
%      save('handles.mat', 'handles');
%save handles;

% --- Executes on button press in ButttonExit.
function ButttonExit_Callback(hObject, eventdata, handles)
% hObject    handle to ButttonExit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;

%%%%%%%%%%%%%%%% SUPPORT FUNCTION [1]
function str = getCurrentPopupString(hh)
%# getCurrentPopupString returns the currently selected string in the popupmenu with handle hh

%# could test input here
if ~ishandle(hh) || strcmp(get(hh,'Type'),'popupmenu')
error('getCurrentPopupString needs a handle to a popupmenu as input')
end

%# get the string - do it the readable way
list = get(hh,'String');
val = get(hh,'Value');
if iscell(list)
   str = list{val};
else
   str = list(val,:);
end

%%%%%%%%%%%%%%%%%SUPPORT FUNCTION [2]
function BytesAvailable_Callback(obj,event) 
load handles
persistent  t0;
persistent  nPts;
persistent  xTime;
persistent  yDataCH1;
 
persistent  nDataCH1Save;
persistent  nDataCH2Save;
 
data = fread(obj,obj.BytesAvailable);
t0 = evalin('base', 't0');
if t0==0
    nPts = 38000;	% number of points to display on stripchart
    xTime = ones(1,nPts)*NaN;
    yDataCH1 = ones(1,nPts)*NaN;
end
lenFrameData=length(data);
% khoi tao data
nIRData = [];
nRData = [];
 % tach data thanh cac luong du lieu
synIndex = 1; % contro data
while (synIndex < (lenFrameData - 12))
     if ((data(synIndex)==255)&&(data(synIndex+1)==255)&&(data(synIndex+2)==0)&&(data(synIndex+3)==0)&&(data(synIndex+4)==255))
         nIRData = [nIRData (data(synIndex+8)*256+data(synIndex+7))]; % IR truoc
         nRData = [nRData  (data(synIndex+10)*256+data(synIndex+9))];% R  sau
         synIndex = synIndex + 11; % bo qua khung vua roi
     else
         synIndex = synIndex + 1;  % do dong bo
     end
     
end
%--------------save Data--------------------------------------------
nDataCH1Save=[nDataCH1Save nIRData];
nDataCH2Save=[nDataCH2Save nRData];
  
assignin('base', 'nDataCH1Save', nDataCH1Save); % tao trong workspace
assignin('base', 'nDataCH2Save', nDataCH2Save); % tao trong workspace
lenData = length(nRData); % du lieu that data

% Update the plot, initial t0=0 in workspace
% t1=length(TimeSecond);
time=t0:1:t0+lenData-1;
t0=t0+lenData-1; % update thoi diem luc sau
assignin('base', 't0', t0); % tao trong workspace

xTime(1:end-lenData) = xTime(lenData+1:end);  % shift old data left
xTime(end-lenData+1:end) = time;        % new data goes on right

% yPos1 = get(handles.yPos1,'value');
% yDataCH1(1:end-lenData) = yDataCH1(lenData+1:end);  % shift old data left
% yDataCH1(end-lenData+1:end) = (yPos1 + 2.5) + 120*nDataCH1f0DC*2.5/32767;%(nDataf0DC*2.5/32768)*2;        % new data goes on right 75
yDataCH1(1:end-lenData) = yDataCH1(lenData+1:end);
assignin('base', 'yDataCH1', yDataCH1); % tao trong workspace
assignin('base', 'yDataCH2', yDataCH2); % tao trong workspace
assignin('base', 'xTime', xTime); % tao trong workspace
 
xmin=min(xTime);
xmax=max(xTime);
 
if (xmin==NaN)
    xmax=1;
    xmin=0;
end
set(handles.axes1,'NextPlot','add'); % lenh hold on trong GUI
     %set(handles.axes1,'NextPlot','new'); % lenh new
    
     plot(handles.axes1,xTime,yDataCH1,'b-',xTime,yDataCH2,'r-','LineWidth',2) ;
     
     %plot(handles.axes1,xTime,(yDataCH1+yDataCH2),'r-','LineWidth',2) ;
     %plot(handles.axes1,xTime,yDataCH2,'r-') ;

     axis(handles.axes1,[xmax-4800 xmax -10 10]);
     %axis(handles.axes1,[xmin xmax -1 1]);

     drawnow                                 % refresh display
     % refreshdata(handles.axes1);
     save('handles.mat', 'handles');  

%--- Executes on button press in ButtonSave.
function ButtonSave_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% evalin('base','dataall');
load handles;
global dataall;
uisave('dataall.mat','dataall');


% --- Executes on selection change in ListChan.
function ListChan_Callback(hObject, eventdata, handles)
% hObject    handle to ListChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ListChan contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ListChan


% --- Executes during object creation, after setting all properties.
function ListChan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ListChan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopSPS.
function PopSPS_Callback(hObject, eventdata, handles)
% hObject    handle to PopSPS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopSPS contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopSPS


% --- Executes during object creation, after setting all properties.
function PopSPS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopSPS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in PopFilType1.
function PopFilType1_Callback(hObject, eventdata, handles)
% hObject    handle to PopFilType1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopFilType1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopFilType1


% --- Executes during object creation, after setting all properties.
function PopFilType1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopFilType1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TextFc1_Callback(hObject, eventdata, handles)
% hObject    handle to TextFc1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TextFc1 as text
%        str2double(get(hObject,'String')) returns contents of TextFc1 as a double


% --- Executes during object creation, after setting all properties.
function TextFc1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TextFc1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopFilType2.
function PopFilType2_Callback(hObject, eventdata, handles)
% hObject    handle to PopFilType2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopFilType2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopFilType2


% --- Executes during object creation, after setting all properties.
function PopFilType2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopFilType2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TextOrder_Callback(hObject, eventdata, handles)
% hObject    handle to TextOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TextOrder as text
%        str2double(get(hObject,'String')) returns contents of TextOrder as a double


% --- Executes during object creation, after setting all properties.
function TextOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TextOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function TextFc2_Callback(hObject, eventdata, handles)
% hObject    handle to TextFc2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TextFc2 as text
%        str2double(get(hObject,'String')) returns contents of TextFc2 as a double


% --- Executes during object creation, after setting all properties.
function TextFc2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TextFc2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ButtonAddFil.
function ButtonAddFil_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonAddFil (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global h;  %impulse response for filter
global af, global bf;
global Type;
fs=str2num(getCurrentPopupString(handles.PopSPS));    %sampling frequency
Order=str2num(get(handles.TextOrder,'String'));
Type1=get(handles.PopFilType1,'Value');
Type2=get(handles.PopFilType2,'Value');
switch Type1
    case 1
        Type = 1;
        wHam = hamming(Order+1);
        switch Type2
            case 1
                fc = str2num(get(handles.TextFc1,'String')); % cutoff frequency
                h  = fir1(Order,2*fc/fs,wHam);
            case 2
                fc = str2num(get(handles.TextFc1,'String')); % cutoff frequency
                h  = fir1(Order,2*fc/fs,'high',wHam);
            case 3
                fc1= str2num(get(handles.TextFc1,'String')); % cutoff frequency
                fc2= str2num(get(handles.TextFc2,'String')); % cutoff frequency
                h  = fir1(Order,2*[fc1 fc2]/fs,wHam);
            case 4
                fc1= str2num(get(handles.TextFc1,'String')); % cutoff frequency
                fc2= str2num(get(handles.TextFc2,'String')); % cutoff frequency
                h  = fir1(Order,2*[fc1 fc2]/fs,'stop',wHam);  
        end
    case 2
        Type = 2;
        switch Type2
            case 1
                fc = str2num(get(handles.TextFc1,'String'));
                [bf,af]=newbutter(Order,2*fc/fs,'low')
            case 2
                fc = str2num(get(handles.TextFc1,'String'));
                [bf,af]=newbutter(Order,2*fc/fs,'high');
            case 3
                fc1= str2num(get(handles.TextFc1,'String')); % cutoff frequency
                fc2= str2num(get(handles.TextFc2,'String')); % cutoff frequency
                [bf,af]=newbutter(Order,2*[fc1 fc2]/fs);
            case 4
                fc1= str2num(get(handles.TextFc1,'String')); % cutoff frequency
                fc2= str2num(get(handles.TextFc2,'String')); % cutoff frequency
                [bf,af]=newbutter(Order,2*[fc1 fc2]/fs,'stop');
        end
end
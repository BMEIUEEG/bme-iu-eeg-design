y=[];
Fs = 250;                    % Sampling frequency
T = 1/Fs;                     % Sample time
L = length(data1);                     % Length of signal
% t = (0:L-1)*T;                % Time vector
% Sum of a 50 Hz sinusoid and a 120 Hz sinusoid
%x = 0.7*sin(2*pi*50*t) + sin(2*pi*120*t); 
% x = cos(3*pi*(t)) + (.5 * (cos(2*pi*(t))));
y = data1*4.5/(2^23-1)-4.5;  %+ 2*randn(size(t));     % Sinusoids plus noise
title('Signal Corrupted with Zero-Mean Random Noise')
xlabel('time (milliseconds)')
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(y,NFFT)/L;
f = Fs/2*linspace(0,1,NFFT/2+1);
plot(y)
figure;
% Plot single-sided amplitude spectrum.
plot(f,2*abs(Y(1:NFFT/2+1))) 
title('Single-Sided Amplitude Spectrum of y(t)')
xlabel('Frequency (Hz)')
ylabel('|Y(f)|')
%
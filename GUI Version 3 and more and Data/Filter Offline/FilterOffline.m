function varargout = FilterOffline(varargin)
% FILTEROFFLINE MATLAB code for FilterOffline.fig
%      FILTEROFFLINE, by itself, creates a new FILTEROFFLINE or raises the existing
%      singleton*.
%
%      H = FILTEROFFLINE returns the handle to a new FILTEROFFLINE or the handle to
%      the existing singleton*.
%
%      FILTEROFFLINE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in FILTEROFFLINE.M with the given input arguments.
%
%      FILTEROFFLINE('Property','Value',...) creates a new FILTEROFFLINE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before FilterOffline_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to FilterOffline_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help FilterOffline

% Last Modified by GUIDE v2.5 22-Jun-2014 23:22:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @FilterOffline_OpeningFcn, ...
                   'gui_OutputFcn',  @FilterOffline_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before FilterOffline is made visible.
function FilterOffline_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to FilterOffline (see VARARGIN)

% Choose default command line output for FilterOffline
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes FilterOffline wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = FilterOffline_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in ButtonBrowse.
function ButtonBrowse_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonBrowse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;data=[];global fplots; fplots=1;
[filename pathname]=uigetfile({'*.mat'},'File Selector');
fullpath=strcat(pathname,filename);
structdata=load(fullpath);
celldata=struct2cell(structdata);
data=celldata{1,1};
% data=data1(30:end)*4.5/(2^23-1)-4.5;
assignin('base','data',data);
set(handles.PathName,'String',fullpath);
% axis(handles.axes1,'XLim',[1 length(data)]);

% --- Executes on button press in ButtonPlot.
function ButtonPlot_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonPlot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;
% plot(handles.axes1,data*4.5/(2^23-1)-4.5);
plot(handles.axes1,data);

% --- Executes on button press in ButtonFFT.
function ButtonFFT_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonFFT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;global fs;global yfil;
global fplots;
FFTPlot=get(handles.PopChooseData,'Value');
switch FFTPlot
    case 1
        dataplot=data;
    case 2
        dataplot=yfil;
end
fs=str2num(getCurrentPopupString(handles.PopSPS)); %sampling Frequency 
L=length(dataplot);
n=pow2(nextpow2(L));
Fdata=fft(dataplot,L);
Fdata=Fdata.*conj(Fdata)/n;
datax=(0:n-1)*(fs/n);
% FFTdata=fft(data,L); %FFT
% Fdata=FFTdata.*conj(FFTdata)/L; %Magnitude Spectrum
% datax=fs*(0:L/2)/L;
% plot(handles.axes2,datax(1:length(datax)),Fdata(1:length(datax)));
% plot(handles.axes2,datax,Fdata(1:length(datax)));
plot(handles.axes2,datax(fplots:floor(n/2)),Fdata(fplots:floor(n/2)));

% --- Executes on button press in ButtonFilter.
function ButtonFilter_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global data;global fs;global yfil;
Fc=get(handles.GetFc,'String');
Noder=get(handles.GetOrder,'String');
Fc=str2num(Fc);
Noder=str2num(Noder);
Type=get(handles.PopFilterType,'Value');
switch Type
    case 1
        d=fdesign.highpass('N,Fc',Noder,Fc,fs);
    case 2
        d=fdesign.lowpass('N,Fc',Noder,Fc,fs);
end
Hd=design(d);
yfil=filter(Hd,data);
plot(handles.axes3,yfil);

% --- Executes on button press in ButtonPlus.
function ButtonPlus_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fplots;
fplots=fplots+1;
set(handles.Fplots,'String',num2str(fplots));


% --- Executes on button press in ButtonMinus.
function ButtonMinus_Callback(hObject, eventdata, handles)
% hObject    handle to ButtonMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fplots;
if fplots>0
    fplots=fplots-1;
end
set(handles.Fplots,'String',num2str(fplots));

%%%%%%%%%%%%%%%% SUPPORT FUNCTION [1]
function str = getCurrentPopupString(hh)
%# getCurrentPopupString returns the currently selected string in the popupmenu with handle hh
%# could test input here
if ~ishandle(hh) || strcmp(get(hh,'Type'),'popupmenu')
error('getCurrentPopupString needs a handle to a popupmenu as input')
end

%# get the string - do it the readable way
list = get(hh,'String');
val = get(hh,'Value');
if iscell(list)
   str = list{val};
else
   str = list(val,:);
end


function PathName_Callback(hObject, eventdata, handles)
% hObject    handle to PathName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PathName as text
%        str2double(get(hObject,'String')) returns contents of PathName as a double


% --- Executes during object creation, after setting all properties.
function PathName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PathName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in PopFilterType.
function PopFilterType_Callback(hObject, eventdata, handles)
% hObject    handle to PopFilterType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopFilterType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopFilterType


% --- Executes during object creation, after setting all properties.
function PopFilterType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopFilterType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function GetFc_Callback(hObject, eventdata, handles)
% hObject    handle to GetFc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of GetFc as text
%        str2double(get(hObject,'String')) returns contents of GetFc as a double


% --- Executes during object creation, after setting all properties.
function GetFc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to GetFc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function GetOrder_Callback(hObject, eventdata, handles)
% hObject    handle to GetOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of GetOrder as text
%        str2double(get(hObject,'String')) returns contents of GetOrder as a double


% --- Executes during object creation, after setting all properties.
function GetOrder_CreateFcn(hObject, eventdata, handles)
% hObject    handle to GetOrder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in PopChooseData.
function PopChooseData_Callback(hObject, eventdata, handles)
% hObject    handle to PopChooseData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopChooseData contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopChooseData


% --- Executes during object creation, after setting all properties.
function PopChooseData_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopChooseData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ButtonPlus_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ButtonPlus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes during object creation, after setting all properties.
function ButtonMinus_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ButtonMinus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on selection change in PopSPS.
function PopSPS_Callback(hObject, eventdata, handles)
% hObject    handle to PopSPS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns PopSPS contents as cell array
%        contents{get(hObject,'Value')} returns selected item from PopSPS


% --- Executes during object creation, after setting all properties.
function PopSPS_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PopSPS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

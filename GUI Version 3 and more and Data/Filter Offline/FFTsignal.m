% close all;
% clear all;
% clc;
fs=250; %sampling frequency
t=0:1/fs:2; %time t
% f1=20; %1st frequency value
% f2=50; %2nd frequency value
% y=cos(2*pi*f1*t)+cos(2*pi*f2*t); %generate signal
y=data1*4.5/(2^23-1)-4.5;
subplot(2,2,1);plot(y);title('original signal'); %plot original signal
L=length(y);
yff=fft(y,L); %Fourier transform
fy=yff.*conj(yff)/L; %magnitude spectrum
fx=fs*(0:L/2)/L; %frequency axis
subplot(2,2,2);plot(fx(700:6400),fy(700:6400));title('FFT y'); %plot frequency domain of the signal
d = fdesign.lowpass('N,Fc',4,30,250); %low pass filter with N order = 20, cut-off frequency=30Hz, sampling frequency fs=250
Hd = design(d); % Uses the FIR window method
yfil=filter(Hd,y); %create filter
subplot(2,2,3);plot(yfil);title('y after filter'); %plot filtered signal
yfilff=fft(yfil,L); %Fourier transform of new signal
fyfil=yfilff.*conj(yfilff)/L;
subplot(2,2,4);plot(fx(2:end),fyfil(2:length(fx)));title('FFT y after filter'); %plot frequency domain of new signal

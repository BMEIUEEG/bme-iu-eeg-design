close all;
clear all;
clc;
fs=250; %sampling frequency
t=0:1/fs:2; %time t
f1=20; %1st frequency value
f2=50; %2nd frequency value
y=cos(2*pi*f1*t)+cos(2*pi*f2*t); %generate signal
subplot(4,1,1);plot(t,y);title('original signal'); %plot original signal
L=length(y);
yff=fft(y,L); %Fourier transform
fy=yff.*conj(yff)/L; %magnitude spectrum
fx=fs*(0:L/2)/L; %frequency axis
subplot(4,1,2);plot(fx,fy(1:length(fx)));title('FFT y'); %plot frequency domain of the signal
d = fdesign.lowpass('N,Fc',20,30,250); %low pass filter with N order = 20, cut-off frequency=30Hz, sampling frequency fs=250
Hd = design(d); % Uses the FIR window method
yfil=filter(Hd,y); %create filter
subplot(4,1,3);plot(t,yfil);title('y after filter'); %plot filtered signal
yfilff=fft(yfil,L); %Fourier transform of new signal
fyfil=yfilff.*conj(yfilff)/L;
subplot(4,1,4);plot(fx,fyfil(1:length(fx)));title('FFT y after filter'); %plot frequency domain of new signal

Order =5;
fc = 3;
fs = 250;
[bf_IIR,af_IIR]=newbutter(Order,2*fc/fs,'high');
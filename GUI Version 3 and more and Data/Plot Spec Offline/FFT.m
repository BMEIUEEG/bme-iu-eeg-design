fs=250;
fplots=2;
L=length(data);

n=pow2(nextpow2(L));
Fdata=fft(data,L);
Fdata=Fdata.*conj(Fdata)/n;
datax=(0:n-1)*(fs/n);
plot(datax(fplots:floor(n/2)),Fdata(fplots:floor(n/2)));
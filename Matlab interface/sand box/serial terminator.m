function real_time_data_stream_plotting
   %script here
bytesToRead = 500;

Portname = 'COM4';
    SerialPort=serial(Portname);
    SerialPort.Baudrate=384000;
    SerialPort.Databits = 8;
    SerialPort.Parity='none';
    SerialPort.Stopbits=1;
    
    %SerialPort.InputBufferSize=50000;
    SerialPort.BytesAvailableFcnCount = bytesToRead;
    %SerialPort.BytesAvailableFcnMode = 'terminator';
    SerialPort.BytesAvailableFcnMode = 'byte';
        
    %Channel=str2num(getCurrentPopupString(handles.ListChan));  %%get Channel to plot
   % SerialPort.BytesAvailableFcn = @readbuffer
    SerialPort.BytesAvailableFcn = {@localReadAndPlot,plotHandle,bytesToRead};

    % Setup a figure window and define a callback function for close operation
figureHandle = figure('NumberTitle','off',...
    'Name','Live Data Stream Plot Serial',...
    'Color',[0 0 0],...
    'CloseRequestFcn',{@localCloseFigure,interfaceObject});
    

try
       %SerialPort = SerialPort; % s chinh la handles.s 
       fopen(SerialPort);
       pause(3);
       % hien thi Disconnect
       %set(handles.ButtonConnect, 'String','Disconnect')
       %drawnow;
    
    catch e
           disp('Error');
           fclose(SerialPort);
    end
      
  
function localReadAndPlot(SerialPort,~,figureHandle,bytesToRead)
global dataAll;
%% 
% Read the desired number of data bytes
data = fread(SerialPort,bytesToRead);
dataAll = [dataAll;data];

assignin('base','dataAllout',dataAll);
%% 
% Update the plot
%set(figureHandle,'Ydata',data);

function localCloseFigure(figureHandle,~,interfaceObject)

%% 
% Clean up the interface object
fclose(interfaceObject);
delete(interfaceObject);
clear interfaceObject;

%% 
% Close the figure window
delete(figureHandle);

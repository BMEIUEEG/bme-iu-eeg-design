 function real_time_data_stream_plotting
   %script here
bytesToRead = 500; %number of bytes to read at onnce time from buffer
%{  
%init value
global dataAll;dataAll=[];global datac;datac=[];
global data1;data1=[];global data2;data2=[];
global data3;data3=[];global data4;data4=[];
global data5;data5=[];global data6;data6=[];
global data7;data7=[];global data8;data8=[];
%}

%init serial port
Portname = 'COM4';
    SerialPort=serial(Portname);
    SerialPort.Baudrate=384000;
    SerialPort.Databits = 8;
    SerialPort.Parity='none';
    SerialPort.Stopbits=1;
    SerialPort.InputBufferSize=500000;
    
    SerialPort.BytesAvailableFcnCount = bytesToRead;
    %SerialPort.BytesAvailableFcnMode = 'terminator';
    SerialPort.BytesAvailableFcnMode = 'byte';
     SerialPort.BytesAvailableFcn = {@localReadAndPlot,bytesToRead};

%% init a figure
figureHandle = figure('NumberTitle','off',...
    'Name','Live Data Stream Plot Serial',...
    'Color',[0 0 0],...
    'CloseRequestFcn',{@localCloseFigure,SerialPort});   
 % Setup the axes for the figure
axesHandle = axes('Parent',figureHandle,...
    'YGrid','on',...
    'YColor',[0.9725 0.9725 0.9725],...
    'XGrid','on',...
    'XColor',[0.9725 0.9725 0.9725],...
    'Color',[0 0 0]);
xlabel(axesHandle,'Number of Samples');
ylabel(axesHandle,'Value');

%% Initialize the plot and hold the settings on
hold on;
plotHandle = plot(axesHandle,0,'-y','LineWidth',1);


try
       fopen(SerialPort);
       %pause(3);
catch e
           disp('Error');
           fclose(SerialPort);   
end
%% Call back function
function localReadAndPlot(SerialPort,~,bytesToRead)

%%% Declare variables
global dataall;    %rawdata package stream to computer
global datac;    %n%umber of data send: count packets/frames
global data1;   %channel 1 to 8 raw data extracted from the stream
global data2;
global data3;
global data4;
global data5;
global data6;
global data7;
global data8;   
global L;


% Read the desired number of data bytes from input buffer
data = fread(SerialPort,bytesToRead);       % read binary data from serial port
dataall = [dataall;data];                   % append new data chunk to existing data chunk

%% Prepare of obtaining new data and plotting
%Initialize sysIndex
if ~isempty(datac)
    synIndex=length(data1)*33;  %%use length data 1 to retrive corrupt segment of the previous loop
else
    synIndex=1;                 %%if it is the 1st time to get data, synIndex run from the beginning
    L=length(datac);            %%initial L (length of data=[])=0
    nPts=1000;                  %%number of points to plot
    x=ones(1,nPts)*NaN;         %%prepare for x and y
    y=ones(1,nPts)*NaN;
end

%% seperating packages from the large frame
while ((length(dataall)> synIndex + 32)) % the rest of the read data can include a package or not
    if ((dataall(synIndex)==254)&&(dataall(synIndex+1)==1)) %if yes, check if the the synIndex is at the header location
        data1=[data1; dataall(synIndex+5)*65536+dataall(synIndex+6)*256+dataall(synIndex+7)];
        data2=[data2; dataall(synIndex+8)*65536+dataall(synIndex+9)*256+dataall(synIndex+10)];
        %data3=[data3; dataall(synIndex+11)*65536+dataall(synIndex+12)*256+dataall(synIndex+13)];
        %data4=[data4; dataall(synIndex+14)*65536+dataall(synIndex+15)*256+dataall(synIndex+16)];
        %data5=[data5; dataall(synIndex+17)*65536+dataall(synIndex+18)*256+dataall(synIndex+19)];
        %data6=[data6; dataall(synIndex+20)*65536+dataall(synIndex+21)*256+dataall(synIndex+22)];
        %data7=[data7; dataall(synIndex+23)*65536+dataall(synIndex+24)*256+dataall(synIndex+25)];
        %data8=[data8; dataall(synIndex+26)*65536+dataall(synIndex+27)*256+dataall(synIndex+28)];
        datac=[datac; dataall(synIndex+29)*(2^24)+dataall(synIndex+30)*65536+dataall(synIndex+31)*256+dataall(synIndex+32)];
        synIndex=synIndex+33;
    else 
        synIndex=synIndex+1;
    end
end


%% 
% Update the plot
%set(figureHandle,'Ydata',data);

function localCloseFigure(figureHandle,~,interfaceObject)

%% init global values
global dataall;    %rawdata package stream to computer
global datac;    %n%umber of data send: count packets/frames
global data1;   %channel 1 to 8 raw data extracted from the stream
global data2;
global data3;
global data4;
global data5;
global data6;
global data7;
global data8;   
global L;

% Clean up the interface object

%% assign variable
assignin('base','dataAllout123',dataall);
assignin('base','datac',datac);
assignin('base','data1',data1);
assignin('base','data2',data2);
assignin('base','data3',data3);
assignin('base','data4',data4);
assignin('base','data5',data5);
assignin('base','data6',data6);
assignin('base','data7',data7);
assignin('base','data8',data8);

%% close serial port
fclose(interfaceObject);
delete(interfaceObject);
clear interfaceObject;

%% 
% Close the figure window
delete(figureHandle);

 function real_time_data_stream_plotting_ring_buffer
 
   %script here
bytesToRead = 500; %number of bytes to read at onnce time from buffer
%{  
%init value
global dataAll;dataAll=[];global datac;datac=[];
global data1;data1=[];global data2;data2=[];
global data3;data3=[];global data4;data4=[];
global data5;data5=[];global data6;data6=[];
global data7;data7=[];global data8;data8=[];
%}
%% line buffer - for 10 channel ( 9 channel + the count)
global linebuffSize
global linebuffer_x
global linebuffer_y
global linebuffer_1;global linebuffer_2;global linebuffer_3;global linebuffer_4;global linebuffer_5;global linebuffer_6;global linebuffer_7;global linebuffer_8;global linebuffer_9;
global t;
global h; global h1;global h2;global h3;global h4;global h5;global h6;global h7;global h8;global h9;

linebuffSize = 1000;
linebuffer_x = nan(1,linebuffSize);
linebuffer_y = nan(1,linebuffSize);
%buffer 8 channel
linebuffer_1 = nan(1,linebuffSize);
linebuffer_2 = nan(1,linebuffSize);
linebuffer_3 = nan(1,linebuffSize);
linebuffer_4 = nan(1,linebuffSize);
linebuffer_5 = nan(1,linebuffSize);
linebuffer_6 = nan(1,linebuffSize);
linebuffer_7 = nan(1,linebuffSize);
linebuffer_8 = nan(1,linebuffSize);
linebuffer_9 = nan(1,linebuffSize);
t=1:linebuffSize;                       %x spacing

subplot
h = plot(linebuffer_y);

% Create the pie chart in position 1 of a 2x2 grid
figure
subplot(5, 2, 1)
h1 = plot(t,linebuffer_1);
title('Channel 1')

% Create the bar chart in position 2 of a 2x2 grid
subplot(5, 2, 2)
h2 = plot(t,linebuffer_2);
title('Channel 2')


% Create the stem chart in position 3 of a 2x2 grid
subplot(5, 2, 3)
h3 = plot(t,linebuffer_2);
title('Channel 3')


% Create the line plot in position 4 of a 2x2 grid
subplot(5, 2, 4)
h4 = plot(t,linebuffer_4);
title('Channel 4')

% Create the pie chart in position 1 of a 2x2 grid
subplot(5, 2, 5)
h5 = plot(t,linebuffer_5);
title('Channel 5')

% Create the bar chart in position 2 of a 2x2 grid
subplot(5, 2, 6)
h6 = plot(t,linebuffer_6);
title('Channel 6')


% Create the stem chart in position 3 of a 2x2 grid
subplot(5, 2, 7)
h7 = plot(t,linebuffer_7);
title('Channel 7')

% Create the stem chart in position 3 of a 2x2 grid
subplot(5, 2, 8)
h8 = plot(t,linebuffer_8);
title('Channel 8')

% Create the line plot in position 4 of a 2x2 grid
subplot(5, 2, 9)
h9 = plot(t,linebuffer_9);
title('Channel count')



%init serial port
Portname = 'COM4';
    SerialPort=serial(Portname);
   
                                    

%% init a figure
figureHandle = figure('NumberTitle','off',...
    'Name','Live Data Stream Plot Serial',...
    'Color',[0 0 0],...
    'CloseRequestFcn',{@localCloseFigure,SerialPort});   
 % Setup the axes for the figure
axesHandle = axes('Parent',figureHandle)

xlabel(axesHandle,'Number of Samples');
ylabel(axesHandle,'Value');

%% Initialize the plot and hold the settings on
hold on;
plotHandle = plot(axesHandle,0,'-y','LineWidth',1);


%% Set up serial port
 SerialPort.Baudrate=384000;
    SerialPort.Databits = 8;
    SerialPort.Parity='none';
    SerialPort.Stopbits=1;
    SerialPort.InputBufferSize=5000;
    
    SerialPort.BytesAvailableFcnCount = bytesToRead;
    %SerialPort.BytesAvailableFcnMode = 'terminator';
    SerialPort.BytesAvailableFcnMode = 'byte';
     SerialPort.BytesAvailableFcn = {@localReadAndPlot,plotHandle,bytesToRead};
     
%% init ring buffer
global bufferSize
bufferSize = 10000; % the intterupt of Matlab byteavailable is planed to be 500, here we use 20 times the size of that data as the size of the ringbuffer, unknown optimed size
%bytesToRead = 500;  %number of byte or element to insert to the buffer every time
global packageLength;  % Armbrain 2 header package size (2h+9x3data+4count)
packageLength =33;
global data
data = nan(bufferSize,1)'; %' for transpose
global datastream
datastream = nan(bytesToRead,1); %pre allocate for data
global ind
ind = 1;    %buffer index
global variable;
variable =1;
%init data
global last
last = -1;
global first
first = 1; 

%% open serial port
try
       fopen(SerialPort);
       %pause(3);
catch e
           disp('Error');
           fclose(SerialPort);   
end
%% Call back function
function localReadAndPlot(SerialPort,~,figureHandle,bytesToRead)

%%% Declare variables
global dataall;    %rawdata package stream to computer
global datac;    %n%umber of data send: count packets/frames
global data1;   % 24 bit data from channel 1 to 8 raw data extracted from the serial stream 
global data2;   %
global data3;
global data4;
global data5;
global data6;
global data7;
global data8;   
global L;

%% declare variables for buffer
global datastream
global last
global first
global bufferSize
global ind
global data
global packageLength
global linebuffSize
global linebuffer_x
global linebuffer_y
global linebuffer_1;global linebuffer_2;global linebuffer_3;global linebuffer_4;global linebuffer_5;global linebuffer_6;global linebuffer_7;global linebuffer_8;global linebuffer_9;
global t;
global h; global h1;global h2;global h3;global h4;global h5;global h6;global h7;global h8;global h9;
% Read the desired number of data bytes from input buffer
datastream = fread(SerialPort,bytesToRead);       % read binary data from serial port

   %put datastream in the buffer
    for indDatastream = 1:bytesToRead %5 is the length of the data stream
    last = mod(ind-1, bufferSize)+1;         %calulate the index to put data in. The mod function help to maintain the ring buffer
    data(last) = datastream (indDatastream); %copy data from datastream buffer to ringbuffer
    ind = ind +1;                           %index indicate number of bytes were read
    end    
    
    %condition to analyze data
    % (last > packageLength) : make sure there are enough data to analyze
    % also prevent the half data package
    % (last > packageLength) && first > last : full buffer, last cross over
    % the bufferlimit 1 round before the first
    % (last > packageLength) && (last > first+packageLength) : normal
    % condition
    if (last > packageLength) && (( first > last)||(last > first+packageLength)) 
        
        while (     (last > (first + packageLength)) || (first > last)     )    %condition to make sure the available byte in the buffer is more than 1 package
        %look for new header
            
            % range of first: from 1 to bufferSize. That's why we need to
            % -1 in the modular function, then +1 outside to make sure that
            % we got first = bufferSize value after the mod function
            
            if (data(mod(first-1 +0, bufferSize)+1) == 254) && (data(mod(first-1 + 1, bufferSize)+1) == 1)  %fake header
            %frame header detected, start to analyze data of a frame
            %output_x = [output_x ; data(mod(first-1 + 2, bufferSize)+1)] ;       %2 in the index within a frame of xput meaningful data after the header to output matrix check t
            %===============================*************==============================================
            % A data structure sample
            % Byte 1: Header 1 
            % Byte 2: Header 2
            % Byte 3: Status 1
            % Byte 4: Status 2
            % Byte 5: Status 3
            % Byte 6: Channel 1 Byte 1 
            % Byte 7: Channel 1 Byte 2
            % Byte 8: Channel 1 Byte 3
            % Byte 9: Channel 2 Byte 1
            % Byte 10: Channel 2 Byte 2
            % Byte 11: Channel 2 Byte 3
            %
            %
            %
            %
            %
            %
            %
            %
            %
            %
            %
            %
            %
            %
            %
            % Byte 33: Channel 8 Byte 3
            %============================***********===========================================
            
            %plot channel 1: 5 6 7
            %
            data1 = data(mod(first-1 + 5, bufferSize)+1)*65536 + data(mod(first-1 + 6, bufferSize)+1)*256+data(mod(first-1 + 7, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_1 =[linebuffer_1(2:end) (data1*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            %plot channel 2: 8 9 10
            data2 = data(mod(first-1 + 8, bufferSize)+1)*65536 + data(mod(first-1 + 9, bufferSize)+1)*256+data(mod(first-1 + 10, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_2 =[linebuffer_2(2:end) (data2*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            %plot channel 3: 11 12 13   -temperature channel
            data3 = data(mod(first-1 + 11, bufferSize)+1)*65536 + data(mod(first-1 + 12, bufferSize)+1)*256+data(mod(first-1 + 13, bufferSize)+1);       %combine seperate bytes of channel 1
            data3 = ((data3*4.5/(2^23-1)-4.5)*(10^6) -145300)/490 +25 ;
            
            linebuffer_3 =[linebuffer_3(2:end) data3];  %4 is the location of 16 in dataall. location of 254 is 0
            
            
            %plot channel 4: 14 15 16
            data4 = data(mod(first-1 + 14, bufferSize)+1)*65536 + data(mod(first-1 + 15, bufferSize)+1)*256+data(mod(first-1 + 16, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_4 =[linebuffer_4(2:end) (data4*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            %plot channel 5: 17 18 19
            data5 = data(mod(first-1 + 17, bufferSize)+1)*65536 + data(mod(first-1 + 18, bufferSize)+1)*256+data(mod(first-1 + 19, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_5 =[linebuffer_5(2:end) (data5*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            %plot channel 6: 20 21 22
            data6 = data(mod(first-1 + 20, bufferSize)+1)*65536 + data(mod(first-1 + 21, bufferSize)+1)*256+data(mod(first-1 + 22, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_6 =[linebuffer_6(2:end) (data6*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            
            %plot channel 7: 23 24 25
            data7 = data(mod(first-1 + 23, bufferSize)+1)*65536 + data(mod(first-1 + 24, bufferSize)+1)*256+data(mod(first-1 + 25, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_7 =[linebuffer_7(2:end) (data7*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            %plot channel 8: 26 27 28
            data8 = data(mod(first-1 + 26, bufferSize)+1)*65536 + data(mod(first-1 + 27, bufferSize)+1)*256+data(mod(first-1 + 28, bufferSize)+1);       %combine seperate bytes of channel 1
            linebuffer_8 =[linebuffer_8(2:end) (data8*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
                                   
            %output_y = [output_y ; data(mod(first-1 + 3, bufferSize)+1)] ;
            linebuffer_9 =[linebuffer_9(2:end) data(mod(first-1 + 32, bufferSize)+1)];  %32 is the location of the last counter
           
            first = mod(first-1 +packageLength, bufferSize)+1;                  %completed reading current package, index to the next pakcage    
            else first = mod(first-1 +1, bufferSize)+1 ;
        
            end
        % set(h,'YData',linebuffer_y); 
         set(h1,'YData',linebuffer_1);
         set(h2,'YData',linebuffer_2);
         set(h3,'YData',linebuffer_3);
         set(h4,'YData',linebuffer_4);
         set(h5,'YData',linebuffer_5);
         set(h6,'YData',linebuffer_6);
         set(h7,'YData',linebuffer_7);
         set(h8,'YData',linebuffer_8);   
         set(h9,'YData',linebuffer_9);
        end
%         [m,n] = size(output_x);                 %calculate current size of x matrix to match with t value for the plot     
%         plot(t(1:m),output_x);
        
%       figure;

       

        %        tic
%         set(figureHandle,'XData',t,'YData',linebuffer_x);
%         toc
%        plot(t,linebuffer_x,t,linebuffer_y);
   %    set(figureHandle,'Ydata',linebuffer_x);
 %      drawnow;
        
    end












%dataall = [dataall;data];                   % append new data chunk to existing data chunk

% %% Prepare of obtaining new data and plotting
% %Initialize sysIndex
% if ~isempty(datac)
%     synIndex=length(data1)*33;  %%use length data 1 to retrive corrupt segment of the previous loop
% else
%     synIndex=1;                 %%if it is the 1st time to get data, synIndex run from the beginning
%     L=length(datac);            %%initial L (length of data=[])=0
%     nPts=1000;                  %%number of points to plot
%     x=ones(1,nPts)*NaN;         %%prepare for x and y
%     y=ones(1,nPts)*NaN;
% end
% 
% %% seperating packages from the large frame
% while ((length(dataall)> synIndex + 32)) % the rest of the read data can include a package or not
%     if ((dataall(synIndex)==254)&&(dataall(synIndex+1)==1)) %if yes, check if the the synIndex is at the header location
%         data1=[data1; dataall(synIndex+5)*65536+dataall(synIndex+6)*256+dataall(synIndex+7)];
%         data2=[data2; dataall(synIndex+8)*65536+dataall(synIndex+9)*256+dataall(synIndex+10)];
%         %data3=[data3; dataall(synIndex+11)*65536+dataall(synIndex+12)*256+dataall(synIndex+13)];
%         %data4=[data4; dataall(synIndex+14)*65536+dataall(synIndex+15)*256+dataall(synIndex+16)];
%         %data5=[data5; dataall(synIndex+17)*65536+dataall(synIndex+18)*256+dataall(synIndex+19)];
%         %data6=[data6; dataall(synIndex+20)*65536+dataall(synIndex+21)*256+dataall(synIndex+22)];
%         %data7=[data7; dataall(synIndex+23)*65536+dataall(synIndex+24)*256+dataall(synIndex+25)];
%         %data8=[data8; dataall(synIndex+26)*65536+dataall(synIndex+27)*256+dataall(synIndex+28)];
%         datac=[datac; dataall(synIndex+29)*(2^24)+dataall(synIndex+30)*65536+dataall(synIndex+31)*256+dataall(synIndex+32)];
%         synIndex=synIndex+33;
%     else 
%         synIndex=synIndex+1;
%     end
% end


%% 
% Update the plot
%set(figureHandle,'Ydata',data);

function localCloseFigure(figureHandle,~,interfaceObject)

%% init global values
global dataall;    %rawdata package stream to computer
global datac;    %n%umber of data send: count packets/frames
global data1;   %channel 1 to 8 raw data extracted from the stream
global data2;
global data3;
global data4;
global data5;
global data6;
global data7;
global data8;   
global L;

global linebuffer_1;global linebuffer_2;global linebuffer_3;global linebuffer_4;global linebuffer_5;global linebuffer_6;global linebuffer_7;global linebuffer_8;global linebuffer_9;
global t;
% Clean up the interface object

%% assign variable
assignin('base','dataAllout123',dataall);
%assignin('base','linebuffer_9',linebuffer_1);
assignin('base','linebuffer_1',linebuffer_1);
assignin('base','linebuffer_2',linebuffer_2);
assignin('base','linebuffer_3',linebuffer_3);
assignin('base','linebuffer_4',linebuffer_4);
assignin('base','linebuffer_5',linebuffer_5);
assignin('base','linebuffer_6',linebuffer_6);
assignin('base','linebuffer_7',linebuffer_7);
assignin('base','linebuffer_8',linebuffer_8);

%% close serial port
fclose(interfaceObject);
delete(interfaceObject);
clear interfaceObject;

%% 
% Close the figure window
delete(figureHandle);

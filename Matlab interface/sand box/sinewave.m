%% This snipset will do the following things
%1. Genereate a sinewave(x)
%2. Generate a squrewave (y)
%3. Merge them together in the datastream simdataplot with header [255;1]
%at the beginning of each data package

%Matlab sine wave generator
A = 2;
F0 =2
fSampling = 500;
tSampling = 1/fSampling;
t = 0:tSampling:10; % 1000 sample
xt=A*sin(2*pi*F0* t);

%plot sine and square
stem(t,xt);
plot(t,xt)

yt=square(2*pi*F0* t)
hold on;
plot(t,yt)

%% put all to a data stream simdataplot
% generate a datastream with 254 and 1 as header1 and header 2
% x and y come from the square and sine wave. The extractor in another file
% should be able to extract the sine and the square wave of this string
% out.


simdataplot = nan(20004,1)';
for i =0:5000
    simdataplot(4*i+1) = 254;
    simdataplot(4*i+2) = 1;
    simdataplot(4*i+3) = xt(i+1);
    simdataplot(4*i+4) = yt(i+1);
    i=i+1;
    
end

%extract x databack
for i = 0:5000
extractxt(i+1) = simdataplot(4*i+3);
end
figure;
plot(t,extractxt);



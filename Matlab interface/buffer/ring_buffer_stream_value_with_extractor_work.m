bufferSize = 20000; % the intterupt of Matlab byteavailable is planed to be 500, here we use 20 times the size of that data as the size of the ringbuffer, unknown optimed size
byteavailable = 500;  %number of byte or element to insert to the buffer every time
packageLength = 33;  % Armbrain 2 header package size (2h+9x3data+4count)
data = nan(bufferSize,1)'; %' for transpose
datastream = nan(byteavailable,1); %pre allocate for data
ind = 1;    %buffer index
variable =1;
%init data
last = -1;
first  = 1; 
streamDatasource = dataAllout123;     %have to preopen the datastream sample from Armbrain prior to this program run
                          %equal horizontal space to display data

output_x = []  ; %demo output
output_y = [];

%% line buffer
linebuffSize = 5000;
linebuffer_x = nan(1,linebuffSize);
linebuffer_y = nan(1,linebuffSize);
t=1:linebuffSize; 
%% function
for variable = 1:700
 
   datastream = streamDatasource((variable*byteavailable  - (byteavailable-1)):variable*byteavailable); %take byteavailable value from dataall at a time to put in the buffer
   
   %put datastream in the buffer
    for indDatastream = 1:byteavailable %5 is the length of the data stream
    last = mod(ind-1, bufferSize)+1;         %calulate the index to put data in. The mod function help to maintain the ring buffer
    data(last) = datastream (indDatastream); %copy data from datastream buffer to ringbuffer
    ind = ind +1;                           %index indicate number of bytes were read
    end    
    
    %condition to analyze data
    % (last > packageLength) : make sure there are enough data to analyze
    % also prevent the half data package
    % (last > packageLength) && first > last : full buffer, last cross over
    % the bufferlimit 1 round before the first
    % (last > packageLength) && (last > first+packageLength) : normal
    % condition
    if (last > packageLength) && (( first > last)||(last > first+packageLength)) 
        
        while (     (last > (first + packageLength)) || (first > last)     )    %condition to make sure the available byte in the buffer is more than 1 package
        %look for new header
            if (data(mod(first-1 +0, bufferSize)+1) == 254) && (data(mod(first-1 + 1, bufferSize)+1) == 1)  %fake header
            %frame header detected, start to analyze data of a frame
            %output_x = [output_x ; data(mod(first-1 + 2, bufferSize)+1)] ;       %2 in the index within a frame of xput meaningful data after the header to output matrix check t
            data1 = data(mod(first-1 + 5, bufferSize)+1)*65536 + data(mod(first-1 + 6, bufferSize)+1)*256+data(mod(first-1 + 7, bufferSize)+1);       %combine seperate bytes of channel 1
            
            linebuffer_x =[linebuffer_x(2:end) (data1*4.5/(2^23-1)-4.5)];  %4 is the location of 16 in dataall. location of 254 is 0
            
            %output_y = [output_y ; data(mod(first-1 + 3, bufferSize)+1)] ;
            linebuffer_y =[linebuffer_y(2:end) data(mod(first-1 + 32, bufferSize)+1)];  %32 is the location of the last counter
           
            first = mod(first-1 +packageLength, bufferSize)+1;                  %completed reading current package, index to the next pakcage    
        else first = mod(first-1 +1, bufferSize)+1 ;
        
        end
        end
%         [m,n] = size(output_x);                 %calculate current size of x matrix to match with t value for the plot     
%         plot(t(1:m),output_x);
        plot(t,linebuffer_x,t,linebuffer_y);
%       plot(t,linebuffer_x);
        drawnow;
        
    end
end



%% This is a simple demonstration of ring buffer
%The programe will take data from the dataall file with header 254 and 1,
%put it in indDatastream as a intput buffer, data is the ring buffer with
%buffer size is the size of it

load('data_sample_dataall.mat')
bufferSize = 2000; %(2 package of Armbrain 33 bytes)
byteavailable = 500;  %number of byte or element to insert to the buffer every time
packageLength = 4;  %length of a meaningful package information from serial port
data = nan(bufferSize,1)'; %' for transpose
datastream = nan(byteavailable,1); %pre allocate for data
ind = 1;    %buffer index
variable =30;
%init data
last = -1;
first  = 1;
output_x = [];    %demo output
output_y = [];  
simdataplot = dataall;

for variable = 1:40
    %generate datastream 
    datastream = simdataplot((variable*byteavailable - (byteavailable-1)):variable*byteavailable); %take byteavailable value from dataall at a time to put in the buffer
    %put datastream in the buffer
    for indDatastream = 1:byteavailable %5 is the length of the data stream
    last = mod(ind-1, bufferSize)+1;         %calulate the index to put data in. The mod function help to maintain the ring buffer
    data(last) = datastream (indDatastream); %copy data from datastream buffer to ringbuffer
    ind = ind +1;                           %index indicate number of bytes were read
    end    
    
    %condition to analyze data
    % (last > packageLength) : make sure there are enough data to analyze
    % also prevent the half data package
    % (last > packageLength) && first > last : full buffer, last cross over
    % the bufferlimit 1 round before the first
    % (last > packageLength) && (last > first+packageLength) : normal
    % condition
    if (last > packageLength) && (( first > last)||(last > first+packageLength)) 
        
        while (     (last > (first + packageLength)) || (first > last)     )    %condition to make sure the available byte in the buffer is more than 1 package
        if (data(mod(first-1, bufferSize)+1) == 254) && (data(mod(first-1 + 1, bufferSize)+1) == 1)  %fake header
            
            output_x = [output_x ; data(mod(first-1 + 2, bufferSize)+1)] ;       %2 in the index within a frame of xput meaningful data after the header to output matrix check t
            output_y = [output_y ; data(mod(first-1 + 3, bufferSize)+1)] ;
            first = mod(first-1 +packageLength, bufferSize)+1;                  %completed reading current package, index to the next pakcage    
        else first = mod(first-1 +1, bufferSize)+1 ;
        
        end
        end
        
    end
end



//Golden function

//SPI1 init golden function
void mySPI_Init(void){

	
	GPIO_InitTypeDef GPIO_InitTypeDefStruct; //GPIO structs for SPIx 
	SPI_InitTypeDef SPI_InitTypeDefStruct;   //SPI Struct for SPIx

		
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE); //Enable RCC for SPIx
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA , ENABLE); // Enable RCC for GPIOx Both core and CS pin
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE , ENABLE);
	
	//Innitialize SPIx structure
	SPI_InitTypeDefStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;  
	SPI_InitTypeDefStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitTypeDefStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitTypeDefStruct.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitTypeDefStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
	//SPI1->CR2 = SPI_CR2_SSOE; May need to enable this bit too to make spi work
	SPI_InitTypeDefStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitTypeDefStruct.SPI_CPOL = SPI_CPOL_High;
	SPI_InitTypeDefStruct.SPI_CPHA = SPI_CPHA_2Edge;
	SPI_Init(SPI1, &SPI_InitTypeDefStruct);
	
	
	
	//Initialize GPIOx Structure for SPI core pins
	GPIO_InitTypeDefStruct.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_6 | GPIO_Pin_5;
	GPIO_InitTypeDefStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitTypeDefStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitTypeDefStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitTypeDefStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitTypeDefStruct);
  
	//Define AF function for eachpin
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);
	
	/* configure pins used by SPI1
	 * PA5 = SCK
	 * PA6 = MISO
	 * PA7 = MOSI
	 */
 //Initialize GPIOx for SPIx CS pin
	GPIO_InitTypeDefStruct.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitTypeDefStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitTypeDefStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitTypeDefStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitTypeDefStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOE, &GPIO_InitTypeDefStruct);

	// Set CS line to high => Disable slave for now
	GPIO_SetBits(GPIOE, GPIO_Pin_3);

  //Enable GPIO1 control register
	SPI_Cmd(SPI1, ENABLE);
// SPI_CR1_BR_0 SPI_CR1_SSM  SPI_CR1_BR SPI_CR2_SSOE
}


//Golden function sendata spi1
void mySPI_SendData(uint8_t address, uint8_t data){			// put in address and data to write to

	GPIO_ResetBits(GPIOE, GPIO_Pin_3);

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, address);
	
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, data);
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);

	GPIO_SetBits(GPIOE, GPIO_Pin_3);
	}
}


//Golden getdata spi1
uint16_t mySPI_GetData(uint8_t address){										// put in an address; get data from that address

	
	
	GPIO_ResetBits(GPIOE, GPIO_Pin_3); 											// CS SPI1 enable

	address = 0x80 | address;          											//Change MSB to 1 => SPI read function 

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, address);
	
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);															//Clear RXNE bit

	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE));  //transmit buffer empty?
	SPI_I2S_SendData(SPI1, 0x00);														//Dummy byte to generate clock
	
	while(!SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)); //data received?
	SPI_I2S_ReceiveData(SPI1);															// 
	
	GPIO_SetBits(GPIOE, GPIO_Pin_3); 												//CS line up; Disable slave
	
	return SPI_I2S_ReceiveData(SPI1); //return reveiced data
	
}
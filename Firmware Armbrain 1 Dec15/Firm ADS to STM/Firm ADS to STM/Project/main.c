#include <stm32f4xx.h>
#include <misc.h>
#include "Usart.h"
#include "SysTick.h"
#include "Leds.h"
#include "SPI.h"
#include "ADS_Command.h"
#include <string.h>
#include "stdio.h"
	uint8_t countbytes, countbits,data[10][10],i;
	uint32_t dataChn[10];
	
int main(void) 
{
  //Init_USART(384000); // initialize USART1 @ 9600 baud
	Init_USART(230400);
	Led_Init();
	InitialiseSysTick();
	mySPI_Init();
	
	PowerUp_ADS(); 						// Power up ADS: Delay for system startup and reset ADS1299 chip
	SendADSCommand(0x11,2);		// Send Command SDATA: STOP RDATAC mode
	Recognize_ADS();					// Check the ADS device ID. If the ID is corrent, blink the LED.
	
	WREG_ADS(0x01,0xD5);			//Config1: Daisy Chain Mode, Oscillator output disable, DT 250SPS. 1101 0110 250 normal > 1101 0101 500sps. 6 250. 5 500
	WREG_ADS(0x02,0xC0);			//Config2: Test Signals are driven Externally, Test Signal and Amplitude are Default
	WREG_ADS(0x03,0xE1);			//Config3: Unsing Internal Reference
	WREG_ADS(0x05,0x50);			//Channel 1-8: Normal Operation;PGA=1;SRB2 off;Normal electrode input
	WREG_ADS(0x06,0x50);			// Channel 2: 0110 0000 gain 12
	WREG_ADS(0x07,0x50);			//-
	WREG_ADS(0x08,0x50);			//-
	WREG_ADS(0x09,0x50);			//-
	WREG_ADS(0x0A,0x50);			//-
	WREG_ADS(0x0B,0x50);			//-
	WREG_ADS(0x0C,0x50);
	WREG_ADS(0x0D,0x50);
	WREG_ADS(0x0E,0x50);//-8
	
	//LOFF setup
	WREG_ADS(0x04,0x02);			// 04h LOFF: 000.0.00.10 Current source signal: 31Hz, 6nA > 1010 - 6uA 31Hz
	WREG_ADS(0x0F,0x00);			//Turn on current source for all P channel; turn on channel 1 to 4
	WREG_ADS(0x10,0x00);			// Turn off current source for all N channel
	WREG_ADS(0x11,0x00);			// No flipping current sourcee			
	
	//* Square wave test signal 
/*
	WREG_ADS(0x02,0xD0);			//
	WREG_ADS(0x05,0x05);
	WREG_ADS(0x06,0x05);
	WREG_ADS(0x07,0x05);
	WREG_ADS(0x08,0x05);
	WREG_ADS(0x09,0x05);
	WREG_ADS(0x0A,0x05);
	WREG_ADS(0x0B,0x05);
	WREG_ADS(0x0C,0x05);
*/
	GPIO_SetBits(GPIOB, GPIO_Pin_1); //Enable START, then /DRDY toggle at f_CLK/8192 =>t=4ms
	Delay(10);	
	GPIO_ToggleBits(GPIOC,GPIO_Pin_15);   	
	GPIO_ToggleBits(GPIOC,GPIO_Pin_15);   	
	GPIO_ToggleBits(GPIOC,GPIO_Pin_15);   	
	GPIO_ToggleBits(GPIOC,GPIO_Pin_15);   	
	
	GetData_RDATAC();


/*
	while(1)
	{	
		//Send_Header(0xfe,0x01);
		GetData_RDATAC();
		//GetData_RDATA();							//Retrive Data from RDATA mode
		
	}
*/
}
 

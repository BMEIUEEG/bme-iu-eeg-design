/*
				Read Motion from Accelerometer LIS3DSH then Display Led
*/
#include "main.h"
#include "stm32f4xx.h"
#include "stm32f4xx_spi.h"
#include "stdio.h"
#define MAX_STRLEN 12 // this is the maximum string length of our string in characters
volatile char received_string[MAX_STRLEN+1]; // this will hold the recieved string

uint16_t mySPI_GetData(uint8_t address);
void mySPI_SendData(uint8_t address, uint8_t data);
void mySPI_Init(void);
void Led_Init(void);
void Led_Motion(uint16_t getdataX,uint16_t getdataY,uint16_t getdataZ);
void init_USART1(uint32_t baudrate);
void USART_puts(USART_TypeDef* USARTx, volatile char *s);
void USART1_IRQHandler(void);
void PowerUp_ADS(void);
void SendADSCommand(uint8_t data, uint32_t delaytime);
uint16_t RREG_ADS(uint8_t address);
void WREG_ADS(uint8_t address,uint8_t data);
void GetData_RDATAC(void);


void Delay(__IO uint32_t nTime)
{ 
  
  while(nTime != 0)
		nTime--;
}
uint8_t i;
uint32_t dataCh1, data1=0x77;	
int main(void)
{
	//uint8_t ADS_ID;
	//InitialiseSysTick();
	Led_Init();
	init_USART1(4800);
	mySPI_Init();
	
	//mySPI_SendData(0x20, 0x57); //write to CTRL_REG4(add:0x20):01100111
														  //ODR: 50Hz, BDU:continuous update
															//Zen, Yen, Xen: XYZ enable
    
		//getdataX = mySPI_GetData(0x29);

		//while( !(USART1->SR & 0x00000040));
		//USART_SendData(USART1, getdataX);	
	/*
	PowerUp_ADS();
	SendADSCommand(0x11,2);
	ADS_ID=RREG_ADS(0x00);
	if ((ADS_ID & 0x0f)==0x0e)
	{
		while (i<6)
		{
		GPIO_ToggleBits(GPIOD,GPIO_Pin_12);   		//Display LED to indicate that connection is OK!
		Delay(500000);
		i++;
		}
	}
	WREG_ADS(0x01,0xD6);			//Config1: DT 250SPS
	WREG_ADS(0x02,0xC0);			//Config2
	WREG_ADS(0x03,0xE0);			//Config3: Unsing Internal Reference
	WREG_ADS(0x05,0x00);			//Channel 1-8: Normal Operation;PGA=1;SRB2 off;Normal electrode input
	WREG_ADS(0x06,0x00);			//-
	WREG_ADS(0x07,0x00);			//-
	WREG_ADS(0x08,0xE1);			//-
	WREG_ADS(0x09,0x00);			//-
	WREG_ADS(0x0A,0x00);			//-
	WREG_ADS(0x0B,0x00);			//-
	WREG_ADS(0x0C,0x00);			//-
	GPIO_SetBits(GPIOB, GPIO_Pin_1); //Enable START
	SendADSCommand(0x10,2);		//Put ADS in RDATAC mode (need 4 t_CLK~2us)
	*/
	while(1)
	{
			//while(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_4)!=0x00);
			//GetData_RDATAC();
		  USART_puts(USART1, "Check Tenten!! rn");
	}

		//Delay(1000000); // = 1s
		//GPIO_ToggleBits(GPIOD,GPIO_Pin_12);	
}
	


////------------Function------------
//SPI

void Led_Init(void)
{
	//Initialize 4LED on STM32F4 for checking
	GPIO_InitTypeDef Led_Init;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	Led_Init.GPIO_Pin = GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15; // we want to configure all LED GPIO pins
	Led_Init.GPIO_Mode = GPIO_Mode_OUT; 		// we want the pins to be an output
	Led_Init.GPIO_Speed = GPIO_Speed_50MHz; 	// this sets the GPIO modules clock speed
	Led_Init.GPIO_OType = GPIO_OType_PP; 	// this sets the pin type to push / pull (as opposed to open drain)
	Led_Init.GPIO_PuPd = GPIO_PuPd_NOPULL; 	// this sets the pullup / pulldown resistors to be inactive
	GPIO_Init(GPIOD, &Led_Init); 			// this finally passes all the values to the GPIO_Init function which takes care of setting the corresponding bits.
	//GPIO_SetBits(GPIOD, GPIO_Pin_15);
}
//Function to Display Motion by Leds
void Led_Motion(uint16_t getdataX,uint16_t getdataY,uint16_t getdataZ)
{
if (getdataX ==0x0000)
		{
			GPIO_ResetBits(GPIOD, GPIO_Pin_12);
		}
		if (getdataY ==0x00FF)
		{
			GPIO_ResetBits(GPIOD, GPIO_Pin_15);
		}
		if (getdataZ ==0x0042)
		{
			GPIO_ResetBits(GPIOD, GPIO_Pin_14);
		}
		else if((getdataX > 0x00C0)&&(getdataX <0x00FD)&&(getdataZ<0x0040))
		{
				GPIO_ToggleBits(GPIOD,GPIO_Pin_12);
		}
		else if((getdataX > 0x0002)&&(getdataX <0x0050)&&(getdataZ<0x0040))
		{
				GPIO_ToggleBits(GPIOD,GPIO_Pin_14);
		}
		else if((getdataY > 0x00B0)&&(getdataY <0x00FD)&&(getdataZ<0x0040))
		{
				GPIO_ToggleBits(GPIOD,GPIO_Pin_15);
		}
		else if((getdataY > 0x0002)&&(getdataX <0x0030)&&(getdataZ<0x0040))
		{
				GPIO_ToggleBits(GPIOD,GPIO_Pin_13);
		}		
		Delay(100);		//=]]]]] that's why it tranfers too slowly!
}
//USART
///USART Initialize	
void init_USART1(uint32_t baudrate)
{

	/* This is a concept that has to do with the libraries provided by ST
	 * to make development easier the have made up something similar to
	 * classes, called TypeDefs, which actually just define the common
	 * parameters that every peripheral needs to work correctly
	 *
	 * They make our life easier because we don't have to mess around with
	 * the low level stuff of setting bits in the correct registers
	 */
	GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
	USART_InitTypeDef USART_InitStruct; // this is for the USART1 initilization
	NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)

	/* enable APB2 peripheral clock for USART1
	 * note that only USART1 and USART6 are connected to APB2
	 * the other USARTs are connected to APB1
	 */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	/* enable the peripheral clock for the pins used by
	 * USART1, PA9 for TX and PA10 for RX
	 */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	/* This sequence sets up the TX and RX pins
	 * so they work correctly with the USART1 peripheral
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; // Pins 9 (TX) and 10 (RX) are used
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// this activates the pullup resistors on the IO pins
	GPIO_Init(GPIOA, &GPIO_InitStruct);					// now all the values are passed to the GPIO_Init() function which sets the GPIO registers

	/* The RX and TX pins are now connected to their AF
	 * so that the USART1 can take over control of the
	 * pins
	 */
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); //
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);

	/* Now the USART_InitStruct is used to define the
	 * properties of USART1
	 */
	USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;// we want the data frame size to be 8 bits (standard)
	USART_InitStruct.USART_StopBits = USART_StopBits_1;		// we want 1 stop bit (standard)
	USART_InitStruct.USART_Parity = USART_Parity_No;		// we don't want a parity bit (standard)
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; // we want to enable the transmitter and the receiver
	USART_Init(USART1, &USART_InitStruct);					// again all the properties are passed to the USART_Init function which takes care of all the bit setting


	/* Here the USART1 receive interrupt is enabled
	 * and the interrupt controller is configured
	 * to jump to the USART1_IRQHandler() function
	 * if the USART1 receive interrupt occurs
	 */
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); // enable the USART1 receive interrupt

	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;		 // we want to configure the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;// this sets the priority group of the USART1 interrupts
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;		 // this sets the subpriority inside the group
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			 // the USART1 interrupts are globally enabled
	NVIC_Init(&NVIC_InitStructure);							 // the properties are passed to the NVIC_Init function which takes care of the low level stuff

	// finally this enables the complete USART1 peripheral
	USART_Cmd(USART1, ENABLE);
}
//USART Function to send character
void USART_puts(USART_TypeDef* USARTx, volatile char *s)
{

	while(*s){
		// wait until data register is empty
		while( !(USARTx->SR & 0x00000040) );
		USART_SendData(USARTx, *s);
		*s++;
	}
}
// this is the interrupt request handler (IRQ) for ALL USART1 interrupts
void USART1_IRQHandler(void)
{

	// check if the USART1 receive interrupt flag was set
	if( USART_GetITStatus(USART1, USART_IT_RXNE) ){

		static uint8_t cnt = 0; // this counter is used to determine the string length
		char t = USART1->DR; // the character from the USART1 data register is saved in t

		/* check if the received character is not the LF character (used to determine end of string)
		 * or the if the maximum string length has been been reached
		 */
		if( (t != 'n') && (cnt < MAX_STRLEN) ){
			received_string[cnt] = t;
			cnt++;
		}
		else{ // otherwise reset the character count er and print the received string
			cnt = 0;
			USART_puts(USART1, received_string);
		}
	}
}

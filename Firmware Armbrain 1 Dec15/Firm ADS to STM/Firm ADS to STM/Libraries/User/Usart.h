#ifndef __Usart_H
#define __Usart_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "system_stm32f4xx.h"
#include <stm32f4xx.h>
#include <misc.h>			 
#include <stm32f4xx_usart.h>
#include <SysTick.h>
/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void Init_USART(uint32_t baudrate);
void USART_puts(USART_TypeDef* USARTx, volatile char *s);
void USART1_IRQHandler(void);
void Send_Data_uint24 (uint32_t data);
void Send_Data_uint32 (uint32_t data);
void Send_Header(uint16_t head1, uint16_t head2);
#endif /* __Usart_H */

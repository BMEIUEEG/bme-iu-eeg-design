/* Blink without Delay

Based on Blink without Delay

http://www.arduino.cc/en/Tutorial/BlinkWithoutDelay
*/

// constants won't change. Used here to set pin numbers:
  // Pin 13: Arduino has an LED connected on pin 13
  // Pin 11: Teensy 2.0 has the LED on pin 11
  // Pin 6: Teensy++ 2.0 has the LED on pin 6
const int led1 =  13;      // the number of the LED pin
const int led2 =  2;       // the number of the second LED
const int led3 =  3;       //3rd
const int led4 =  4;       //4th
const int led5 =  5;       //5th
// Variables will change:
int ledState1 = LOW;             // ledState used to set the LED
int ledState2 = LOW;             // ledState used to set the LED
int ledState3 = LOW;             // ledState used to set the LED
int ledState4 = LOW;             // ledState used to set the LED
int ledState5 = LOW;             // ledState used to set the LED
long previousMillis1 = 0;        // will store last time LED was updated
long previousMillis2 = 0;        // will store last time LED was updated
long previousMillis3 = 0;        // will store last time LED was updated
long previousMillis4 = 0;        // will store last time LED was updated
long previousMillis5 = 0;        // will store last time LED was updated
// Set up desire frequency
int f2 = 8;
int f3 = 8;
int f4 = 8;
int f5 = 8;
// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
unsigned long interval1;            // interval at which to blink (milliseconds)
unsigned long interval2;            // interval at which to blink (milliseconds)
unsigned long interval3;           // interval at which to blink (milliseconds)
unsigned long interval4;           // interval at which to blink (milliseconds)
unsigned long interval5;           // interval at which to blink (milliseconds)

void setup() {
  // set the digital pin as output:
pinMode(led1, OUTPUT);  
pinMode(led2, OUTPUT); 
pinMode(led3, OUTPUT); 
pinMode(led4, OUTPUT); 
pinMode(led5, OUTPUT);

interval2 = 1000000/(f2*2);
interval3 = 1000000/(f3*2);
interval4 = 1000000/(f4*2);
interval5 = 1000000/(f5*2);
}

void loop()
{

  unsigned long currentMillis = micros(); //use micro second to improve accuracy
/*
  if(currentMillis - previousMillis1 > interval1) {
    // save the last time you blinked the LED 
    previousMillis1 = currentMillis;   
    digitalWrite(led1, ledState1 = !ledState1);
   
  }
 */ 
   if(currentMillis - previousMillis2 > interval2) {
    // save the last time you blinked the LED 
    previousMillis2 = currentMillis;   

  digitalWrite(led2, ledState2 = !ledState2);
  }
   if(currentMillis - previousMillis3 > interval3) {
    // save the last time you blinked the LED 
    previousMillis3 = currentMillis;   
   digitalWrite(led3, ledState3 = !ledState3);
  }
   if(currentMillis - previousMillis4 > interval4) {
    // save the last time you blinked the LED 
    previousMillis4 = currentMillis;   
    digitalWrite(led4, ledState4 = !ledState4);
    // set the LED with the ledState of the variable:
    digitalWrite(led4, ledState4);
  }
   if(currentMillis - previousMillis5 > interval5) {
    // save the last time you blinked the LED 
    previousMillis5 = currentMillis;   
    digitalWrite(led5, ledState5 = !ledState5);
  }
  
}
